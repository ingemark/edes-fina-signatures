#!/bin/bash

set -e

MAVEN_USER_HOME=$1
MAVEN_USERNAME=$2
MAVEN_PASSWORD=$3

if [ -z "$MAVEN_USER_HOME" ]
then
  echo "Maven user home not set"
  exit 1
fi

if [ -z "$MAVEN_USERNAME" ]
then
  echo "Maven username not set"
  exit 1
fi

if [ -z "$MAVEN_PASSWORD" ]
then
  echo "Maven password not set"
  exit 1
fi

echo "creating directories"

mkdir -p $MAVEN_USER_HOME/.m2


echo "writing master password"

ENCRYPTED_MASTER_PASSWORD="$(mvn --encrypt-master-password $MAVEN_PASSWORD)"
cat > $MAVEN_USER_HOME/.m2/settings-security.xml <<EOL
<settingsSecurity>
        <master>$ENCRYPTED_MASTER_PASSWORD</master>
</settingsSecurity>
EOL



echo "writing settings"

ENCRYPTED_PASSWORD="$(mvn --encrypt-password $MAVEN_PASSWORD)"

cat > $MAVEN_USER_HOME/.m2/settings.xml <<EOL

<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
        https://maven.apache.org/xsd/settings-1.0.0.xsd">

        <localRepository/>
        <interactiveMode/>
        <usePluginRegistry/>
        <offline/>
        <pluginGroups/>
        <servers>
                <server>
                        <id>ingemark-release</id>
                        <username>$MAVEN_USERNAME</username>
                        <password>$ENCRYPTED_PASSWORD</password>
                </server>
        </servers>
        <mirrors/>
        <proxies/>
        <profiles/>
        <activeProfiles/>
</settings>
EOL






