node {

  emailRecipients = 'tb2b-devel@ingemark.com'

  try {

    build()

    notificationBuildSuccessful()

  } catch(err) {
    notificationBuildFailed()

    throw err
  }
}

def build() {

  resolveProperties()

  withCredentials([
    string(credentialsId: 'sonarUrl', variable: 'SONAR_URL'),
    string(credentialsId: 'nexusIngemarkReleaseRepositoryUrl', variable: 'NEXUS_URL'),
    string(credentialsId: 'jenkinsMavenCache', variable: 'MVN_SERVER_REPO_HOME'),
    usernamePassword(credentialsId: 'nexusCredentials', passwordVariable: 'NEXUS_PASSWORD', usernameVariable: 'NEXUS_USERNAME')
  ]) {
    withEnv([
      'MVN_GUEST_REPO_HOME=/opt/maven/cache',
      'MAVEN_USER_HOME=/tmp/maven/home',
      'MAVEN_OPTS=-Xmx256m -Dmaven.repo.local=/opt/maven/cache -Duser.home=/tmp/maven/home '
      ]) {

      docker.image('repository.ingemark.com/ingemark/tb2b-build-env:5').inside("-v ${MVN_SERVER_REPO_HOME}:${MVN_GUEST_REPO_HOME}") {

        stage('Initialize') {
          checkout scm
          sh 'etc/maven/generate-settings.sh ${MAVEN_USER_HOME} ${NEXUS_USERNAME} ${NEXUS_PASSWORD}'
        }

        stage('Build') {
          sh 'mvn -Dspring.profiles.active=test,jenkins clean verify install'
        }

      if (env.BRANCH_NAME == "master") {
          stage('Analyze code') {
              withSonarQubeEnv('dockerizedSonarqubeRazvoj') {
                  ARTIFACT_ID = sh(script: "mvn help:evaluate -Dexpression=project.artifactId | egrep -v 'INFO|Downloading|Downloaded'", returnStdout: true).trim()
                  GROUP_ID = sh(script: "mvn help:evaluate -Dexpression=project.groupId | egrep -v 'INFO|Downloading|Downloaded'", returnStdout: true).trim()
                  sh "mvn clean sonar:sonar -Dsonar.host.url=${SONAR_URL} -Dsonar.projectKey=${GROUP_ID}:${ARTIFACT_ID}:${BRANCH_NAME} -Dsonar.projectName=${ARTIFACT_ID}:${BRANCH_NAME} -Dsonar.java.binaries=./target"
              }
          }
      }

        if (env.BRANCH_NAME == "master" && params.releaseStrategy == 'RELEASE') {
          stage('Deploy') {
            sh './nexus-deploy-artifacts.sh ${NEXUS_URL} ${NEXUS_USERNAME} ${NEXUS_PASSWORD} ${BUILD_NUMBER}'
          }
        }
      }
    }
  }
}

def notificationBuildFailed() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  mail to: emailRecipients,
  subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has FAILED",
  body: "Please go to ${BUILD_URL} for details."
}

def notificationBuildSuccessful() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  if( currentBuild.previousBuild == null ) {
    return
  }

  if (currentBuild.previousBuild.result == 'FAILURE') {
    mail to: emailRecipients,
    subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has RECOVERED",
    body: "Please go to ${BUILD_URL} for details."
  }
}

def resolveProperties() {
  def config = []


  // make sure cleanup is done on a regular basis
  config.add(
    buildDiscarder(
      logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '30', numToKeepStr: '2')
    )
  )

  config.add(
    disableConcurrentBuilds()
  )

  config.add(
    parameters([
      choice(
        name: 'releaseStrategy',
        choices: 'BUILD\nRELEASE',
        defaultValue: 'BUILD',
        description: 'determines if artifact should be released or not',
      )
    ])
  )

  if( env.BRANCH_NAME == "master" ) {
    config.add(
      pipelineTriggers([cron('H H(21-23) * * *')])
    )
  }

  properties(config)
}