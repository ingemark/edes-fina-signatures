This project demonstrates how to sign UBL documents using signing algorithms supported by FINA.
FINA currently supports 3 algorithms:

* XMLDSign for B2B (hr.ingemark.edes.fina.sig.impl.fina.ubl.xmldsig.y2017.FinaDsigSignatureServiceImpl)
* XADES for B2G and B2B for EN16931 norm (hr.ingemark.edes.fina.sig.impl.fina.ubl.xmldsig.b2g.xades.FinaXadesImpl)

Examples of Xades signature use can be found in class hr.ingemark.edes.fina.sig.signature.XadesSignatureTest.
Examples of XmlDsig signature use can be found in class hr.ingemark.edes.fina.sig.signature.XmlDsigSignatureTest.

Signature validation examples can be found in package hr.ingemark.edes.fina.sig.validation.*.