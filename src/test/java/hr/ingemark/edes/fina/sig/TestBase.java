package hr.ingemark.edes.fina.sig;

import hr.ingemark.edes.fina.sig.ex.TestingException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Before;

public class TestBase {

    protected static final String CONST_TEMP_FILE_LOCATION = "tmp/test-file.xml";
    protected static final String CONST_DIRECTORIES_TMP = "tmp";

    protected static void prepareTemporaryDirectory() {

        Path path = Paths.get(CONST_DIRECTORIES_TMP);

        try {
            // create temporary file in case it does not exists
            if (!Files.exists(path)) {
                Files.createDirectory(Paths.get(CONST_DIRECTORIES_TMP));
            }
        } catch (IOException e) {
            throw new TestingException("Unable to create tmp directory at path: " + path);
        }
    }

    @Before
    public void beforeTest() {
        deleteTmpFile();
    }

    private void deleteTmpFile() {
        File tmpFile = Paths.get(CONST_TEMP_FILE_LOCATION).toFile();

        if(tmpFile.exists()) {
            boolean result = Paths.get(CONST_TEMP_FILE_LOCATION).toFile().delete();
            if(!result) throw new TestingException("Cannot delete temporary file");
        }
    }

}
