package hr.ingemark.edes.fina.sig.ex;

public class TestingException extends RuntimeException {

    public TestingException(String s) {
        super(s);
    }

    public TestingException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
