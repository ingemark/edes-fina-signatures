package hr.ingemark.edes.fina.sig.signature.util;

import java.io.FileInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import static org.assertj.core.api.Assertions.assertThat;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SignatureChecker {

    private SignatureChecker() {
        // util class
    }

    public static void check(String documentLocation) throws Exception {
        Document document = parse(documentLocation);

        NodeList signatureList = document.getElementsByTagName("Signature");
        assertThat(signatureList.getLength()).isEqualTo(1);

        Node signatureNode = signatureList.item(0);
        assertThat(signatureNode.getAttributes().getNamedItem("xmlns").getNodeValue()).isEqualTo("http://www.w3.org/2000/09/xmldsig#");

        NodeList subjectNameList = document.getElementsByTagName("X509SubjectName");
        assertThat(subjectNameList.getLength()).isEqualTo(1);

        Node subjectName = subjectNameList.item(0);
        assertThat(subjectName.getTextContent().replaceAll(" ", "")).isEqualTo("CN=THT,OU=THT,O=Development,L=Zagreb,ST=Zagreb,C=HR");
    }

    private static Document parse(String documentLocation) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new FileInputStream(documentLocation));
    }
}
