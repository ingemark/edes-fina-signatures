package hr.ingemark.edes.fina.sig.signature;

import hr.ingemark.edes.fina.sig.DigitalSignerBuilder;
import hr.ingemark.edes.fina.sig.TestBase;
import hr.ingemark.edes.fina.sig.enums.NormType;
import hr.ingemark.edes.fina.sig.enums.SignatureType;
import hr.ingemark.edes.fina.sig.interfaces.DigitalSigner;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class XmlDsigSignatureTest extends TestBase {

    @Test
    public void must_sign_hr_invoice_reminder_using_b2b_xml_dsig_successfully_when_already_signed() throws Exception {
        DigitalSigner signer = makeDigitalSigner();

        prepareTemporaryDirectory();

        signer.sign("etc/reminders/reminder-hrinvoice-signed.xml", CONST_TEMP_FILE_LOCATION);
        assertThat(Paths.get(CONST_TEMP_FILE_LOCATION)).exists();

        ensureThereIsOnlyOneSignature();
    }

    @Test
    public void must_sign_hr_invoice_reminder_using_b2b_xml_dsig_successfully() throws Exception {
        DigitalSigner signer = makeDigitalSigner();

        prepareTemporaryDirectory();

        signer.sign("etc/reminders/reminder-hrinvoice.xml", CONST_TEMP_FILE_LOCATION);
        assertThat(Paths.get(CONST_TEMP_FILE_LOCATION)).exists();

        ensureThereIsOnlyOneSignature();
    }


    @Test
    public void must_sign_hr_invoice_credit_note_using_b2b_xml_dsig_successfully_when_already_signed() throws Exception {
        DigitalSigner signer = makeDigitalSigner();

        prepareTemporaryDirectory();

        signer.sign("etc/creditnotes/credit-note-hrinvoice-signed.xml", CONST_TEMP_FILE_LOCATION);
        assertThat(Paths.get(CONST_TEMP_FILE_LOCATION)).exists();

        ensureThereIsOnlyOneSignature();
    }

    @Test
    public void must_sign_hr_invoice_credit_note_using_b2b_xml_dsig_successfully() throws Exception {
        DigitalSigner signer = makeDigitalSigner();

        prepareTemporaryDirectory();

        signer.sign("etc/creditnotes/credit-note-hrinvoice.xml", CONST_TEMP_FILE_LOCATION);
        assertThat(Paths.get(CONST_TEMP_FILE_LOCATION)).exists();

        ensureThereIsOnlyOneSignature();
    }

    @Test
    public void mustSignAlreadySignedDocumentUsingB2bXmlDsigSuccessfully() throws Exception {
        DigitalSigner signer = makeDigitalSigner();

        prepareTemporaryDirectory();

        signer.sign(
                "etc/invoices/invoice-hrinvoice-signed.xml",
                CONST_TEMP_FILE_LOCATION);

        assertThat(Paths.get(CONST_TEMP_FILE_LOCATION)).exists();

        ensureThereIsOnlyOneSignature();
    }

    @Test
    public void mustSignDocumentUsingB2bXmlDsigSuccessfully() throws Exception {
        DigitalSigner signer = makeDigitalSigner();

        prepareTemporaryDirectory();

        signer.sign(
                "etc/invoices/invoice-hrinvoice-e1.xml",
                CONST_TEMP_FILE_LOCATION);

        assertThat(Paths.get(CONST_TEMP_FILE_LOCATION)).exists();

        ensureThereIsOnlyOneSignature();
    }

    private DigitalSigner makeDigitalSigner() {
        return DigitalSignerBuilder
                .builder()
                .withKeystore()
                .withFilename("etc/keystores/demo-keystore-2019.p12")
                .withPassword("keystore")
                .withAlias("default")
                .build()
                .withNormType(NormType.HR_INVOICE)
                .withSignatureType(SignatureType.XML_DSIG_2017)
                .build();
    }

    private void ensureThereIsOnlyOneSignature() throws Exception {
        Document document = parse();

        NodeList signatureList = document.getElementsByTagName("Signature");
        assertThat(signatureList.getLength()).isEqualTo(1);

        Node signatureNode = signatureList.item(0);
        assertThat(signatureNode.getAttributes().getNamedItem("xmlns").getNodeValue()).isEqualTo("http://www.w3.org/2000/09/xmldsig#");

        NodeList subjectNameList = document.getElementsByTagName("X509SubjectName");
        assertThat(subjectNameList.getLength()).isEqualTo(1);

        Node subjectName = subjectNameList.item(0);
        assertThat(subjectName.getTextContent()).isEqualTo("CN=THT,OU=THT,O=Development,L=Zagreb,ST=Zagreb,C=HR");
    }

    private Document parse() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new FileInputStream(CONST_TEMP_FILE_LOCATION));
    }
}
