package hr.ingemark.edes.fina.sig.validation;

import hr.ingemark.edes.fina.sig.TestBase;
import hr.ingemark.edes.fina.validation.SignatureValidationReport;
import hr.ingemark.edes.fina.validation.SignatureValidator;
import hr.ingemark.edes.fina.validation.SignatureValidatorBuilder;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UnsignedDocumentValidationTest extends TestBase {

    @Test
    public void accept_invoice_when_not_signed_if_allowed() {
        SignatureValidator specialValidator = SignatureValidatorBuilder
                .builder()
                .withKeystore()
                .withFilename("etc/keystores/demo-truststore-2019.p12")
                .withPassword("keystore")
                .buildKeystore()
                .acceptUnsignedDocument()
                .build();

        SignatureValidationReport report = specialValidator.validate("etc/invoices/invoice-en16931-e1.xml");
        assertThat(report).isNotNull();
        assertThat(report.getErrorReports()).isNotNull();
        assertThat(report.getErrorReports()).hasSize(0);
        assertThat(report.isValid()).isTrue();
    }
}
