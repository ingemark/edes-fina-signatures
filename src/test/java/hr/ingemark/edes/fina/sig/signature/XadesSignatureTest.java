package hr.ingemark.edes.fina.sig.signature;

import hr.ingemark.edes.fina.sig.DigitalSignerBuilder;
import hr.ingemark.edes.fina.sig.enums.NormType;
import hr.ingemark.edes.fina.sig.enums.SignatureType;
import hr.ingemark.edes.fina.sig.interfaces.DigitalSigner;
import hr.ingemark.edes.fina.sig.TestBase;
import hr.ingemark.edes.fina.sig.signature.util.SignatureChecker;
import hr.ingemark.edes.fina.validation.SignatureValidationReport;
import hr.ingemark.edes.fina.validation.SignatureValidator;
import hr.ingemark.edes.fina.validation.SignatureValidatorBuilder;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class XadesSignatureTest extends TestBase {

    private SignatureValidator validator;

    @Before
    public void setUpValidator() {
        this.validator = SignatureValidatorBuilder
                .builder()
                .withKeystore()
                .withFilename("etc/keystores/demo-truststore-2019.p12")
                .withPassword("keystore")
                .buildKeystore()
                .build();
    }

    @Test
    public void mustSignInvoiceUsingXadesSuccessfully() {
        signUsingXadesAndValidateSignature("etc/invoices/invoice-en16931-e1.xml");
    }

    @Test
    public void mustSignAlreadySignedInvoiceUsingXadesSuccessfully() throws Exception {
        signAlreadySignedDocumentUsingXadesAndValidateSignature("etc/invoices/invoice-en16931-signed.xml");
    }

    @Test
    public void mustSignCreditNoteUsingXadesSuccessfully() {
        signUsingXadesAndValidateSignature("etc/creditnotes/credit-note-en16931.xml");
    }

    @Test
    public void mustSignAlreadySignedCreditNoteUsingXadesSuccessfully() throws Exception {
        signAlreadySignedDocumentUsingXadesAndValidateSignature("etc/creditnotes/credit-note-en16931-signed.xml");
    }

    @Test
    public void mustSignOrderUsingXadesSuccessfully() {
        signUsingXadesAndValidateSignature("etc/orders/order-ubl2.1.xml");
    }

    @Test
    public void mustSignAlreadySignedOrderUsingXadesSuccessfully() throws Exception {
        signAlreadySignedDocumentUsingXadesAndValidateSignature("etc/orders/order-ubl2.1-signed.xml");
    }

    @Test
    public void mustSignOrderCancellationUsingXadesSuccessfully() {
        signUsingXadesAndValidateSignature("etc/orders/order-cancellation-ubl2.1.xml");
    }

    @Test
    public void mustSignAlreadySignedOrderCancellationUsingXadesSuccessfully() throws Exception {
        signAlreadySignedDocumentUsingXadesAndValidateSignature("etc/orders/order-cancellation-ubl2.1-signed.xml");
    }

    @Test
    public void mustSignQuotationUsingXadesSuccessfully() {
        signUsingXadesAndValidateSignature("etc/quotations/quotation-ubl2.1.xml");
    }

    @Test
    public void mustSignAlreadySignedQuotationUsingXadesSuccessfully() throws Exception {
        signAlreadySignedDocumentUsingXadesAndValidateSignature("etc/quotations/quotation-ubl2.1-signed.xml");
    }

    @Test
    public void mustSignReminderUsingXadesSuccessfully() {
        signUsingXadesAndValidateSignature("etc/reminders/reminder-ubl2.1.xml");
    }

    @Test
    public void mustSignAlreadySignedReminderUsingXadesSuccessfully() throws Exception {
        signAlreadySignedDocumentUsingXadesAndValidateSignature("etc/reminders/reminder-ubl2.1-signed.xml");
    }

    @Test
    public void mustSignDespatchAdviceUsingXadesSuccessfully() {
        signUsingXadesAndValidateSignature("etc/advices/despatch-advice-ubl2.1.xml");
    }

    @Test
    public void mustSignAlreadySignedDespatchAdviceUsingXadesSuccessfully() throws Exception {
        signAlreadySignedDocumentUsingXadesAndValidateSignature("etc/advices/despatch-advice-ubl2.1-signed.xml");
    }

    private void signUsingXadesAndValidateSignature(String sourceFile) {
        DigitalSigner signer = getDigitalSigner("etc/keystores/demo-keystore-2019.p12");

        prepareTemporaryDirectory();

        signer.sign(
                sourceFile,
                CONST_TEMP_FILE_LOCATION);

        assertThat(Paths.get(CONST_TEMP_FILE_LOCATION)).exists();

        SignatureValidationReport report = validator.validate(CONST_TEMP_FILE_LOCATION);
        assertThat(report.isValid()).isTrue();
    }

    private void signAlreadySignedDocumentUsingXadesAndValidateSignature(String sourceFile) throws Exception {
        signUsingXadesAndValidateSignature(sourceFile);
        SignatureChecker.check(CONST_TEMP_FILE_LOCATION);
    }

    private DigitalSigner getDigitalSigner(String keystoreFilename) {
        return DigitalSignerBuilder
                .builder()
                .withKeystore()
                    .withFilename(keystoreFilename)
                    .withPassword("keystore")
                    .withAlias("default")
                    .build()
                .withNormType(NormType.EN_16931)
                .withSignatureType(SignatureType.XML_XADES_B2G)
                .build();
    }

}
