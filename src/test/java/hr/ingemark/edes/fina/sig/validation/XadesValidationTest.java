package hr.ingemark.edes.fina.sig.validation;

import hr.ingemark.edes.fina.sig.DigitalSignerBuilder;
import hr.ingemark.edes.fina.sig.enums.NormType;
import hr.ingemark.edes.fina.sig.enums.SignatureType;
import hr.ingemark.edes.fina.sig.interfaces.DigitalSigner;
import hr.ingemark.edes.fina.sig.TestBase;
import hr.ingemark.edes.fina.validation.SignatureValidationReport;
import hr.ingemark.edes.fina.validation.SignatureValidator;
import hr.ingemark.edes.fina.validation.SignatureValidatorBuilder;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;

public class XadesValidationTest extends TestBase {

    private static final Logger logger = LoggerFactory.getLogger(XadesValidationTest.class);

    private SignatureValidator validator;

    @Before
    public void setUpValidator() {
        this.validator = SignatureValidatorBuilder
                .builder()
                .withKeystore()
                .withFilename("etc/keystores/demo-truststore-2019.p12")
                .withPassword("keystore")
                .buildKeystore()
                .build();
    }

    // invoice - EN16931
    @Test
    public void invoice_en16931_xades_b2g_validate_successfully() {

        prepareTemporaryDirectory();

        DigitalSigner signer = DigitalSignerBuilder
                .builder()
                .withKeystore()
                .withFilename("etc/keystores/demo-keystore-2019.p12")
                .withPassword("keystore")
                .withAlias("default")
                .build()
                .withNormType(NormType.EN_16931)
                .withSignatureType(SignatureType.XML_XADES_B2G)
                .build();

        String sourceFilename = "etc/invoices/invoice-en16931-e1.xml";
        String targetFilename = String.format("%s/invoice-%s-e1-%s-signed.xml",
                CONST_DIRECTORIES_TMP, NormType.EN_16931, SignatureType.XML_XADES_B2G);


        signer.sign(sourceFilename, targetFilename);

        SignatureValidationReport report = validator.validate(targetFilename);

        assertThat(report.isValid()).isTrue();
        assertThat(report.getAllErrorMessages()).isEqualTo("");
    }


	@Test
	public void invoice_en16931_xades_b2g_validate_successfully_without_X509SKI_and_with_different_digest_algorithm() {

    String sourceFilename =
        "src/test/resources/valid/invoice-en16931-xades-signed-dm-sha1-no-ski.xml";

		SignatureValidationReport report = validator.validate(sourceFilename);

		logger.info(report.getAllErrorMessages());

		assertThat(report.isValid()).isTrue();
		assertThat(report.getAllErrorMessages()).isEqualTo("");
	}


    @Test
    public void invoice_en16931_xades_b2g_report_all_validation_errors() {

        SignatureValidationReport report = validator.validate(
                "src/test/resources/invalid/invoice-en16931-e1-xades-signed-with-errors.xml");

        logger.info(report.getAllErrorMessages());

        assertThat(report.isValid()).isFalse();
        assertThat(report.getErrorReports().size()).isEqualTo(8);

        assertThat(report.getErrorReports().get(0).getErrorMessage()).
                isEqualTo("Reference : XPath element not found");
        assertThat(report.getErrorReports().get(1).getErrorMessage()).isEqualTo("DigestValue : Element is empty");
        assertThat(report.getErrorReports().get(2).getErrorMessage()).isEqualTo("Reference : URI is empty");
        assertThat(report.getErrorReports().get(3).getErrorMessage())
                .isEqualTo("DigestMethod : Algorithm attribute does not match");
        assertThat(report.getErrorReports().get(4).getErrorMessage())
                .isEqualTo("X509SubjectName : Element not found.");
        assertThat(report.getErrorReports().get(5).getErrorMessage()).isEqualTo("X509Certificate : Element is empty");
        assertThat(report.getErrorReports().get(6).getErrorMessage()).isEqualTo(
                "SigningTime : Incorrect signing time, must be in format yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        assertThat(report.getErrorReports().get(7).getErrorMessage()).isEqualTo(
                "DigestValue : Element X509Certificate is empty - cannot validate signing certificate digest value");
    }

    @Test
    public void invoice_en16931_xades_b2g_report_signature_validation_error() {
        SignatureValidationReport report = validator.validate(
                "src/test/resources/invalid/invoice-en16931-e1-xades-signed-with-signatureValue-error.xml");

        logger.info(report.getAllErrorMessages());

        assertThat(report.isValid()).isFalse();
        assertThat(report.getErrorReports().size()).isEqualTo(1);

        assertThat(report.getErrorReports().get(0).getErrorMessage())
                .isEqualTo("SignatureValue : Document signature is invalid");
    }

    // credit_note - EN16931
    @Test
    public void credit_note_en16931_xades_b2g_validate_successfully() {

        prepareTemporaryDirectory();

        DigitalSigner signer = DigitalSignerBuilder
                .builder()
                .withKeystore()
                .withFilename("etc/keystores/demo-keystore-2019.p12")
                .withPassword("keystore")
                .withAlias("default")
                .build()
                .withNormType(NormType.EN_16931)
                .withSignatureType(SignatureType.XML_XADES_B2G)
                .build();

        String sourceFilename = "etc/creditnotes/credit-note-en16931.xml";
        String targetFilename = String.format("%s/credit-note-%s-%s-signed.xml",
                CONST_DIRECTORIES_TMP, NormType.EN_16931, SignatureType.XML_XADES_B2G);


        signer.sign(sourceFilename, targetFilename);

        SignatureValidationReport report = validator.validate(targetFilename);

        assertThat(report.isValid()).isTrue();
        assertThat(report.getAllErrorMessages()).isEqualTo("");

    }

    @Test
    public void credit_note_en16931_xades_b2g_report_all_validation_errors() {

        SignatureValidationReport report = validator.validate(
                "src/test/resources/invalid/credit-note-en16931-xades-signed-with-errors.xml");

        logger.info(report.getAllErrorMessages());

        assertThat(report.isValid()).isFalse();
        assertThat(report.getErrorReports().size()).isEqualTo(8);

        assertThat(report.getErrorReports().get(0).getErrorMessage()).
                isEqualTo("Reference : XPath element not found");
        assertThat(report.getErrorReports().get(1).getErrorMessage()).isEqualTo("DigestValue : Element is empty");
        assertThat(report.getErrorReports().get(2).getErrorMessage()).isEqualTo("Reference : URI is empty");
        assertThat(report.getErrorReports().get(3).getErrorMessage())
                .isEqualTo("DigestMethod : Algorithm attribute does not match");
        assertThat(report.getErrorReports().get(4).getErrorMessage())
                .isEqualTo("X509SubjectName : Element not found.");
        assertThat(report.getErrorReports().get(5).getErrorMessage()).isEqualTo("X509Certificate : Element is empty");
        assertThat(report.getErrorReports().get(6).getErrorMessage()).isEqualTo(
                "SigningTime : Incorrect signing time, must be in format yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        assertThat(report.getErrorReports().get(7).getErrorMessage()).isEqualTo(
                "DigestValue : Element X509Certificate is empty - cannot validate signing certificate digest value");

    }

    @Test
    public void credit_note_en16931_xades_b2g_report_signature_validation_error() {
        SignatureValidationReport report = validator.validate(
                "src/test/resources/invalid/credit-note-en16931-xades-signed-with-signature-value-error.xml");

        logger.info(report.getAllErrorMessages());

        assertThat(report.isValid()).isFalse();
        assertThat(report.getErrorReports().size()).isEqualTo(1);

        assertThat(report.getErrorReports().get(0).getErrorMessage())
                .isEqualTo("SignatureValue : Document signature is invalid");
    }


    // reminder - EN16931
    @Test
    public void reminder_en16931_xades_b2g_validate_successfully() {

        prepareTemporaryDirectory();

        DigitalSigner signer = DigitalSignerBuilder
                .builder()
                .withKeystore()
                .withFilename("etc/keystores/demo-keystore-2019.p12")
                .withPassword("keystore")
                .withAlias("default")
                .build()
                .withNormType(NormType.EN_16931)
                .withSignatureType(SignatureType.XML_XADES_B2G)
                .build();

        String sourceFilename = "etc/reminders/reminder-en16931.xml";
        String targetFilename = String.format("%s/reminder-%s-%s-signed.xml",
                CONST_DIRECTORIES_TMP, NormType.EN_16931, SignatureType.XML_XADES_B2G);


        signer.sign(sourceFilename, targetFilename);

        SignatureValidationReport report = validator.validate(targetFilename);

        assertThat(report.isValid()).isTrue();
        assertThat(report.getAllErrorMessages()).isEqualTo("");

    }

    @Test
    public void reminder_en16931_xades_b2g_report_all_validation_errors() {

        SignatureValidationReport report = validator.validate(
                "src/test/resources/invalid/reminder-en16931-xades-signed-with-errors.xml");

        logger.info(report.getAllErrorMessages());

        assertThat(report.isValid()).isFalse();
        assertThat(report.getErrorReports().size()).isEqualTo(8);

        assertThat(report.getErrorReports().get(0).getErrorMessage()).
                isEqualTo("Reference : XPath element not found");
        assertThat(report.getErrorReports().get(1).getErrorMessage()).isEqualTo("DigestValue : Element is empty");
        assertThat(report.getErrorReports().get(2).getErrorMessage()).isEqualTo("Reference : URI is empty");
        assertThat(report.getErrorReports().get(3).getErrorMessage())
                .isEqualTo("DigestMethod : Algorithm attribute does not match");
        assertThat(report.getErrorReports().get(4).getErrorMessage())
                .isEqualTo("X509SubjectName : Element not found.");
        assertThat(report.getErrorReports().get(5).getErrorMessage()).isEqualTo("X509Certificate : Element is empty");
        assertThat(report.getErrorReports().get(6).getErrorMessage()).isEqualTo(
                "SigningTime : Incorrect signing time, must be in format yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        assertThat(report.getErrorReports().get(7).getErrorMessage()).isEqualTo(
                "DigestValue : Element X509Certificate is empty - cannot validate signing certificate digest value");

    }

    @Test
    public void reminder_en16931_xades_b2g_report_signature_validation_error() {

        SignatureValidationReport report = validator.validate(
                "src/test/resources/invalid/reminder-en16931-xades-signed-with-signature-value-error.xml");

        logger.info(report.getAllErrorMessages());

        assertThat(report.isValid()).isFalse();
        assertThat(report.getErrorReports().size()).isEqualTo(1);

        assertThat(report.getErrorReports().get(0).getErrorMessage())
                .isEqualTo("SignatureValue : Document signature is invalid");

    }
}
