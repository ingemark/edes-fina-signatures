package hr.ingemark.edes.fina.sig.utils;

import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class TakeFirstKeySelector extends KeySelector {

    public KeySelectorResult select(
            KeyInfo keyInfo, Purpose purpose, AlgorithmMethod method, XMLCryptoContext context)
            throws KeySelectorException {

        List<X509Certificate> certificates = new ArrayList<>();

        for (Object keyInfoContent : keyInfo.getContent()) {
            XMLStructure info = (XMLStructure) keyInfoContent;
            if (!(info instanceof X509Data))
                continue;

            X509Data x509Data = (X509Data) info;
            for (Object x509DataContent : x509Data.getContent()) {
                if (!(x509DataContent instanceof X509Certificate))
                    continue;

                final X509Certificate x509Certificate = (X509Certificate) x509DataContent;
                return x509Certificate::getPublicKey;
            }
        }

        throw new KeySelectorException("Missing X509Certificate");
    }
}
