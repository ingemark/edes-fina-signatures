package hr.ingemark.edes.fina.sig.impl.fina.ubl.xmldsig.y2017;

import hr.ingemark.edes.fina.sig.config.KeystoreConfig;
import hr.ingemark.edes.fina.sig.ex.SignatureException;
import hr.ingemark.edes.fina.sig.utils.SignatureUtils;
import hr.ingemark.edes.fina.sig.interfaces.DigitalSigner;
import hr.ingemark.edes.fina.sig.utils.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.crypto.dsig.spec.XPathFilter2ParameterSpec;
import javax.xml.crypto.dsig.spec.XPathType;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FinaDsigSignatureServiceImpl implements DigitalSigner {

    private static final Logger logger = LoggerFactory.getLogger(FinaDsigSignatureServiceImpl.class);

    private static final String FINA_SIGNATURE_XPATH =
            "here()/ancestor::dsig:Signature[0]/../../../../../..//. | " +
                    "here()/ancestor::dsig:Signature[0]/../../../../../..//@* | " +
                    "here()/ancestor::dsig:Signature[0]/../../../../../..//namespace::*";
    private static final String FINA_SIGNATURE_XPATH_DSIG = "dsig";
    private static final String FINA_SIGNATURE_XPATH_NAMESPACE = "http://www.w3.org/2000/09/xmldsig#";
    private static final String CONST_EMPTY_STRING = "";

    private static final String EXTENSIONS_TAG = "UBLExtensions";
    private static final String EXTENSION_TAG = "UBLExtension";
    private static final String EXTENSION_CONTENT_TAG = "ExtensionContent";
    private static final String DOCUMENT_SIGNATURE_TAG = "sig:UBLDocumentSignatures";
    private static final String SIGNATURE_INFORMATION_TAG = "sac:SignatureInformation";

    private static final String CONST_XML_EXT_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2";
    private static final String CONST_XML_SAC_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2";
    private static final String CONST_XML_SIG_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2";
    private static final String CONST_XML_SAC_NS = "xmlns:sac";
    private static final String CONST_XML_SIG_NS = "xmlns:sig";

    private KeystoreConfig keystoreConfig;
    private TransformerFactory transformerFactory;

    public FinaDsigSignatureServiceImpl(KeystoreConfig ksConf) {
        this.keystoreConfig = ksConf;
        this.transformerFactory = SignatureUtils.buildTransformerFactory();
    }

    @Override
    public void sign(String inputFile, String outputFile) {
        byte[] data = sign(inputFile);

        try {
            Files.write(Paths.get(outputFile), data);
        } catch (IOException e) {
            throw new SignatureException("Cannot write document to file", e);
        }
    }

    @Override
    public byte[] sign(String xmlFile) {
        final Document document = XmlUtils.loadXmlFromFile(xmlFile);

        signDocument(document);
        return writeDocumentToByteArray(document);
    }

    @Override
    public byte[] sign(byte[] xmlContent) {
        final Document document = XmlUtils.loadXmlFromBytes(xmlContent);

        signDocument(document);

        return writeDocumentToByteArray(document);
    }

    private byte[] writeDocumentToByteArray(Document document) {

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            this.transformerFactory
                    .newTransformer()
                    .transform(new DOMSource(document), new StreamResult(baos));

            return baos.toByteArray();
        } catch (TransformerException | IOException e) {
            throw new SignatureException("cannot write document to byte array", e);
        }
    }

    private void signDocument(Document document) {
        logger.info("Signing document");

        XMLSignatureFactory sf = XmlUtils.getXMLSignatureFactory();

        SignedInfo signedInfo = getSignedInfo(sf);
        KeyInfo keyInfo = generateKeyInfo(keystoreConfig.getCertificate(), sf);

        signDocument(sf, signedInfo, keyInfo, document);

        logger.info("Document signed");
    }

    private SignedInfo getSignedInfo(XMLSignatureFactory sf) {
        List<Reference> references = getSignEntireDocumentReference(sf);

        CanonicalizationMethod canonicalizationMethod = XmlUtils.getExclusiveWithCommentsCanonicalizationMethod(sf);

        SignatureMethod signatureMethod = XmlUtils.getSha1SignatureMethod(sf);

        return sf.newSignedInfo(canonicalizationMethod, signatureMethod, references);
    }

    private KeyInfo generateKeyInfo(X509Certificate cert, XMLSignatureFactory sf) {
        KeyInfoFactory keyInfoFactory = sf.getKeyInfoFactory();

        List<Object> x509Content = new ArrayList<>();
        x509Content.add(cert);
        x509Content.add(cert.getSubjectX500Principal().getName());

        X509Data x509Data = keyInfoFactory.newX509Data(x509Content);

        return keyInfoFactory.newKeyInfo(Collections.singletonList(x509Data));
    }

    private void signDocument(XMLSignatureFactory sf, SignedInfo si, KeyInfo keyInfo, Document document) {

        try {
            removeExistingSignature(document);
            Node signedNode = createSignatureNode(document);

            DOMSignContext dsc = new DOMSignContext(this.keystoreConfig.getPrivateKey(), signedNode);
            XMLSignature signature = sf.newXMLSignature(si, keyInfo);

            signature.sign(dsc);
        } catch (MarshalException | XMLSignatureException e) {
            throw new SignatureException("cannot sign document", e);
        }
    }

    private void removeExistingSignature(Document document) {
        NodeList signatures = document.getElementsByTagNameNS(CONST_XML_SIG_VALUE, "UBLDocumentSignatures");

        if (signatures.getLength() > 0) {
            Node parentNode = signatures.item(0).getParentNode().getParentNode();
            parentNode.getParentNode().removeChild(parentNode);
        }
    }

    private static Node createSignatureNode(Document document) {
        Element extensionsElement = (Element) document.getDocumentElement()
                .getElementsByTagNameNS(CONST_XML_EXT_VALUE, EXTENSIONS_TAG)
                .item(0);

        Element extensionNode = document.createElementNS(extensionsElement.getNamespaceURI(), EXTENSION_TAG);

        extensionNode.setAttribute(CONST_XML_SAC_NS, CONST_XML_SAC_VALUE);
        extensionNode.setAttribute(CONST_XML_SIG_NS, CONST_XML_SIG_VALUE);

        Node extensionContentNode = document.createElementNS(extensionsElement.getNamespaceURI(), EXTENSION_CONTENT_TAG);
        Node sigNode = document.createElement(DOCUMENT_SIGNATURE_TAG);
        Node sacSigNode = document.createElement(SIGNATURE_INFORMATION_TAG);

        sigNode.appendChild(sacSigNode);
        extensionContentNode.appendChild(sigNode);
        extensionNode.appendChild(extensionContentNode);
        extensionsElement.appendChild(extensionNode);

        return sacSigNode;
    }


    private List<Reference> getSignEntireDocumentReference(XMLSignatureFactory sf) {

        DigestMethod sha1DigestMethod = XmlUtils.getSha1DigestMethod(sf);

        Transform referenceTransformOne = getFinaXPath2Transform(sf);
        Transform referenceTransformTwo = XmlUtils.getEnvelopedTransform(sf);
        Transform referenceTransformThree = XmlUtils.getXmlExcC14dTransform(sf);

        Reference reference = sf.newReference(
                CONST_EMPTY_STRING,
                sha1DigestMethod,
                Arrays.asList(referenceTransformOne, referenceTransformTwo, referenceTransformThree), null, null);

        return Collections.singletonList(reference);
    }

    private Transform getFinaXPath2Transform(XMLSignatureFactory signatureFactory) {

        Map<String, String> xpathNamespaceMap = new HashMap<>();
        xpathNamespaceMap.put(FINA_SIGNATURE_XPATH_DSIG, FINA_SIGNATURE_XPATH_NAMESPACE);

        TransformParameterSpec referenceTransformSpec = new XPathFilter2ParameterSpec(
                Collections.singletonList(
                        new XPathType(FINA_SIGNATURE_XPATH, XPathType.Filter.INTERSECT, xpathNamespaceMap)));

        try {
            return signatureFactory.newTransform(Transform.XPATH2, referenceTransformSpec);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new SignatureException("cannot generate FINA xpath transform", e);
        }
    }
}
