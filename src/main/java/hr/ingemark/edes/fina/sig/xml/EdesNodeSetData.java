package hr.ingemark.edes.fina.sig.xml;

import org.w3c.dom.Node;

import javax.xml.crypto.NodeSetData;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class EdesNodeSetData implements NodeSetData {

    private final List<Node> nodes;

    public EdesNodeSetData(final Node node) {
        this(Collections.singletonList(node));
    }

    public EdesNodeSetData(final List<Node> nodes) {
        Objects.requireNonNull(nodes);

        this.nodes = nodes;
    }

    @Override
    public Iterator iterator() {
        return this.nodes.iterator();
    }
}
