package hr.ingemark.edes.fina.sig.utils;

import hr.ingemark.edes.fina.sig.ex.SignatureException;

import javax.xml.XMLConstants;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;

public class SignatureUtils {

    private SignatureUtils() {
        // util class
    }

    public static TransformerFactory buildTransformerFactory() {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            return transformerFactory;
        } catch (TransformerConfigurationException e) {
            throw new SignatureException("Cannot initialize transformer factory", e);
        }
    }
}
