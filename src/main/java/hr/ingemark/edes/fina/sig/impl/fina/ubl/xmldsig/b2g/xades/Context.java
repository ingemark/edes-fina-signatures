package hr.ingemark.edes.fina.sig.impl.fina.ubl.xmldsig.b2g.xades;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.transform.TransformerFactory;
import java.security.cert.X509Certificate;

public class Context {

    private Document document;

    private String documentType;

    private String signatureId;

    private Element signatureInformationElement;

    private String signedPropertiesId;

    private XMLObject xadesObject;

    private SignedInfo signedInfo;

    private XMLSignatureFactory signatureFactory;

    private TransformerFactory transformerFactory;

    private X509Certificate cert;

    private KeyInfo keyInfo;

    public Document document() {
        return this.document;
    }

    public String documentType() {
        return this.documentType;
    }

    public String signatureId() {
        return this.signatureId;
    }

    public Context signatureId(final String signatureId) {
        this.signatureId = signatureId;
        return this;
    }

    public Element signatureInformationElement() {
        return this.signatureInformationElement;
    }

    public String signedPropertiesId() {
        return signedPropertiesId;
    }

    public Context signedPropertiesId(String signedPropertiesId) {
        this.signedPropertiesId = signedPropertiesId;

        return this;
    }

    public XMLObject xadesObject() {
        return xadesObject;
    }

    public Context xadesObject(XMLObject xadesObject) {
        this.xadesObject = xadesObject;
        return this;
    }

    public SignedInfo signedInfo() {
        return this.signedInfo;
    }

    public XMLSignatureFactory signatureFactory() {
        return this.signatureFactory;
    }

    public TransformerFactory transformerFactory() {
        return this.transformerFactory;
    }

    public X509Certificate cert() {
        return this.cert;
    }

    public KeyInfo keyInfo() {
        return this.keyInfo;
    }

    public Context document(final Document document) {
        this.document = document;
        return this;
    }

    public Context documentType(final String documentType) {
        this.documentType = documentType;
        return this;
    }

    public Context signatureInformationElement(final Element signatureInformationElement) {
        this.signatureInformationElement = signatureInformationElement;
        return this;
    }

    public Context signedInfo(final SignedInfo signedInfo) {
        this.signedInfo = signedInfo;
        return this;
    }

    public Context signatureFactory(final XMLSignatureFactory signatureFactory) {
        this.signatureFactory = signatureFactory;
        return this;
    }

    public Context transformerFactory(final TransformerFactory transformerFactory) {
        this.transformerFactory = transformerFactory;
        return this;
    }

    public Context cert(final X509Certificate cert) {
        this.cert = cert;
        return this;
    }

    public Context keyInfo(final KeyInfo keyInfo) {
        this.keyInfo = keyInfo;
        return this;
    }
}
