package hr.ingemark.edes.fina.sig.examples;

import hr.ingemark.edes.fina.sig.DigitalSignerBuilder;
import hr.ingemark.edes.fina.sig.enums.NormType;
import hr.ingemark.edes.fina.sig.enums.SignatureType;
import hr.ingemark.edes.fina.sig.interfaces.DigitalSigner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class XmlDsigSignerDemo {

    private static final String CONST_TEMP_DIRECTORY = "tmp";

    public static void main(String[] args) throws IOException {
        // please note that this is a self signed certificate useful only in demo purposes
        // for test and production you will need a validation FINA application certificate

        DigitalSigner signer = DigitalSignerBuilder
                .builder()
                .withKeystore()
                .withFilename("etc/keystores/demo-keystore.jks")
                .withPassword("keystore")
                .withAlias("default")
                .build()
                .withNormType(NormType.HR_INVOICE)
                .withSignatureType(SignatureType.XML_DSIG_2017)
                .build();

        prepareTemporaryDirectory();

        signer.sign("etc/invoices/demo-invoice.xml", "tmp/signed-invoice.xml");
    }

    private static void prepareTemporaryDirectory() throws IOException {
        Path path = Paths.get(CONST_TEMP_DIRECTORY);
        // create temporary file in case it does not exists
        if (!path.toFile().exists()) {
            Files.createDirectory(Paths.get("tmp"));
        }
    }
}
