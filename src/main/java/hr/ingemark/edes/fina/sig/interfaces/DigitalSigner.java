package hr.ingemark.edes.fina.sig.interfaces;

public interface DigitalSigner {

    void sign(String sourceFilename, String targetFilename);

    byte[] sign(String xmlFile);

    byte[] sign(byte[] xmlContent);
}
