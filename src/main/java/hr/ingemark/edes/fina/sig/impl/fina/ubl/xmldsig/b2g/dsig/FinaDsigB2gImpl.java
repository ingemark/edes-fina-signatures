package hr.ingemark.edes.fina.sig.impl.fina.ubl.xmldsig.b2g.dsig;

import hr.ingemark.edes.fina.sig.config.KeystoreConfig;
import hr.ingemark.edes.fina.sig.ex.SignatureException;
import hr.ingemark.edes.fina.sig.utils.SignatureUtils;
import hr.ingemark.edes.fina.sig.interfaces.DigitalSigner;
import hr.ingemark.edes.fina.sig.utils.FinaUtils;
import hr.ingemark.edes.fina.sig.utils.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignatureProperties;
import javax.xml.crypto.dsig.SignatureProperty;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.crypto.dsig.spec.XPathFilter2ParameterSpec;
import javax.xml.crypto.dsig.spec.XPathType;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class FinaDsigB2gImpl implements DigitalSigner {

    private static final Logger logger = LoggerFactory.getLogger(FinaDsigB2gImpl.class);

    private static final String CONST_XML_EXT_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2";
    private static final String CONST_XML_INV_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2";
    private static final String CONST_XML_CAC_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2";
    private static final String CONST_XML_SAC_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2";
    private static final String CONST_XML_SIG_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2";

    private static final String CONST_XML_INV_NAME = "inv";
    private static final String CONST_XML_CAC_NAME = "cac";
    private static final String CONST_XML_EXT_NAME = "ext";
    private static final String CONST_XML_SAC_NS = "xmlns:sac";
    private static final String CONST_XML_SIG_NS = "xmlns:sig";
    private static final String CONST_XML_UBL_EXT_NS = "xmlns:ext";

    private static final String CONST_EL_UBL_EXTENSIONS = "UBLExtensions";
    private static final String CONST_EL_UBL_EXTENSION = "ext:UBLExtension";
    private static final String CONST_EL_SIGNATURE_INFORMATION = "sac:SignatureInformation";
    private static final String CONST_EL_UBL_DOCUMENT_SIGNATURES = "sig:UBLDocumentSignatures";
    private static final String CONST_EL_EXTENSION_CONTEXT = "ext:ExtensionContent";

    private KeystoreConfig keystoreConfig;

    public FinaDsigB2gImpl(KeystoreConfig ksConf) {
        this.keystoreConfig = ksConf;
    }

    @Override
    public void sign(String sourceFilename, String targetFilename) {
        byte[] data = sign(sourceFilename);

        try {
            Files.write(Paths.get(targetFilename), data);
        } catch (IOException e) {
            throw new SignatureException("Cannot write document to file", e);
        }
    }

    @Override
    public byte[] sign(String filename) {
        try {
            return sign(Files.readAllBytes(Paths.get(filename)));
        } catch (IOException e) {
            throw new SignatureException("Cannot read file " + filename, e);
        }
    }

    @Override
    public byte[] sign(byte[] xmlContent) {
        Context context = new Context();

        loadXmlContent(context, xmlContent);
        signDocument(context);

        logger.debug("document signed");

        return convertToBytes(context);
    }

    private byte[] convertToBytes(Context context) {
        return XmlUtils.writeDocumentToByteArray(context.document(), context.transformerFactory());
    }

    private void loadXmlContent(Context context, byte[] xmlContent) {
        context.document(XmlUtils.loadXmlFromBytes(xmlContent));
    }

    private void signDocument(Context context)  {

        try {
            generateTransformerFactory(context);
            generateSignatureFactory(context);
            loadCertificate(context);
            generateKeyInfo(context);

            generateSignedPropertiesId(context);

            removeExistingSignature(context);
            addMissingElementsToXml(context);
            reloadXmlAfterModifications(context);

            selectSignatureInformationElement(context);

            createAdobeElement(context);
            createSignedInfo(context);

            sign(context);
        } catch ( Exception e ) {
            throw new SignatureException("Cannot sign document", e);
        }
    }

    private void sign(Context context) throws MarshalException, XMLSignatureException {
        logger.debug("signing document");

        DOMSignContext dsc = new DOMSignContext(this.keystoreConfig.getPrivateKey(), context.signatureInformationElement());

        XMLSignature signature = context.signatureFactory()
                .newXMLSignature(
                        context.signedInfo(),
                        context.keyInfo(),
                        Collections.singletonList(context.adobeObject()),
                        context.signatureId(),
                        null
                );

        signature.sign(dsc);
    }

    private void generateKeyInfo(Context context) {
        logger.debug("Generating key info from provided keystore");

        KeyInfoFactory keyInfoFactory = context.signatureFactory().getKeyInfoFactory();

        List<Object> x509Content = new ArrayList<>();
        x509Content.add(context.cert());

        X509Data x509Data = keyInfoFactory.newX509Data(x509Content);

        context.keyInfo(keyInfoFactory.newKeyInfo(Collections.singletonList(x509Data)));
    }

    private void createSignedInfo(Context context)
            throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {

        logger.debug("Creating signature info");

        CanonicalizationMethod canonicalizationMethod = XmlUtils
                .getExclusiveWithCommentsCanonicalizationMethod(context.signatureFactory());

        SignatureMethod signatureMethod = XmlUtils.getSha1SignatureMethod(context.signatureFactory());

        Map<String, String> namespaces = new HashMap<>();
        namespaces.put(CONST_XML_INV_NAME, CONST_XML_INV_VALUE);
        namespaces.put(CONST_XML_CAC_NAME, CONST_XML_CAC_VALUE);
        namespaces.put(CONST_XML_EXT_NAME, CONST_XML_EXT_VALUE);


        XPathType intersect = new XPathType(
                "here()/ancestor::inv:" + context.documentType() + "[1]",
                XPathType.Filter.INTERSECT,
                namespaces
        );

        XPathType substract = new XPathType(
                "here()/ancestor::ext:UBLExtensions/ext:UBLExtension[last()]",
                XPathType.Filter.SUBTRACT,
                namespaces
        );

        TransformParameterSpec transformDocumentSpec = new XPathFilter2ParameterSpec(
                Arrays.asList(intersect, substract));

        Transform transformDocument = context.signatureFactory().newTransform(Transform.XPATH2, transformDocumentSpec);

        Transform excc14nWithComments = XmlUtils.getXmlExcC14dTransform(context.signatureFactory());
        List<Transform> transforms = Arrays.asList(transformDocument, excc14nWithComments);

        DigestMethod sha1DigestMethod = XmlUtils.getSha1DigestMethod(context.signatureFactory());

        Reference allDocumentReference = context.signatureFactory().newReference(
                "", sha1DigestMethod, transforms, null, null
        );

        Reference signedPropertiesReference = context.signatureFactory().newReference(
                "#" + context.signedPropertyId(),
                sha1DigestMethod,
                Collections.singletonList(excc14nWithComments),
                "http://www.w3.org/2000/09/xmldsig#SignatureProperties",
                null
        );

        context.signedInfo(context
                .signatureFactory()
                .newSignedInfo(
                        canonicalizationMethod,
                        signatureMethod,
                        Arrays.asList(signedPropertiesReference, allDocumentReference)
                )
        );
    }

    private void createAdobeElement(Context context) {

        logger.debug("creating fina's adobe elements");

        // create name element
        Element nameElement = context.document().createElement("Name");

        nameElement.setAttribute("xmlns", "http://ns.adobe.com/pdf/2006");
        nameElement.setTextContent(FinaUtils.extractCnInCertificate(context.cert()));

        DOMStructure nameStructure = new DOMStructure(nameElement);

        SignatureProperty nameProp = context.signatureFactory().newSignatureProperty(
                Collections.singletonList(nameStructure), context.signatureId(), null);

        // create M element
        Element mElement = context.document().createElement("M");

        mElement.setAttribute("xmlns", "http://ns.adobe.com/pdf/2006");
        mElement.setTextContent(FinaUtils.calculateAdobeTimestamp());

        DOMStructure mStructure = new DOMStructure(mElement);

        SignatureProperty mProp = context.signatureFactory().newSignatureProperty(
                Collections.singletonList(mStructure), context.signatureId(), null);

        // add all elements
        SignatureProperties sigProperties = context.signatureFactory().newSignatureProperties(
                Arrays.asList(nameProp, mProp), context.signedPropertyId());

        XMLObject adobeObject = context.signatureFactory().newXMLObject(
                Collections.singletonList(sigProperties),
                null,
                null,
                null
        );

        context.adobeObject(adobeObject);
    }

    private void loadCertificate(Context context) {
        context.cert(this.keystoreConfig.getCertificate());
    }

    private void generateSignatureFactory(Context context) {
        logger.debug("Generating new xml signature factory");

        context.signatureFactory(XmlUtils.getXMLSignatureFactory());
    }

    private void selectSignatureInformationElement(Context context) {
        NodeList nodeList = context.document().getElementsByTagName(CONST_EL_SIGNATURE_INFORMATION);

        Element signatureInformationElement = (Element) nodeList.item(nodeList.getLength() - 1);

        context.signatureInformationElement(signatureInformationElement);
    }

    private void reloadXmlAfterModifications(Context context) {
        logger.debug("Reloading document with modifications");
        // this step is mandatory since without it signature will be invalid
        // not sure what the cause it.
        // It probably has to to with the fact how a transformer works and that we are changing the original
        // document in order to add SignatureInformation
        convertBytesToDocument(context, convertDocumentToBytes(context));
    }

    private void convertBytesToDocument(Context context, byte[] content) {
        context.document(XmlUtils.loadXmlFromBytes(content));
    }

    private byte[] convertDocumentToBytes(Context context) {
        try {
            Transformer transformer = context.transformerFactory().newTransformer();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(context.document()), new StreamResult(baos));

            // clear document so its eligible for gc
            context.document(null);
            return baos.toByteArray();
        } catch (TransformerException e ) {
            throw new SignatureException("Cannot convert document to bytes", e);
        }
    }

    private void generateTransformerFactory(Context context) {
        logger.debug("generating new transformer factory");

        context.transformerFactory(SignatureUtils.buildTransformerFactory());
    }

    private void removeExistingSignature(Context context) {
        Document document = context.document();

        NodeList signatures = document.getElementsByTagNameNS(CONST_XML_SIG_VALUE, "UBLDocumentSignatures");

        if (signatures.getLength() > 0) {
            Node parentNode = signatures.item(0).getParentNode().getParentNode();
            parentNode.getParentNode().removeChild(parentNode);
        }
    }

    private void addMissingElementsToXml(Context context) {
        Document document = context.document();

        Element firstNode = (Element) document.getFirstChild();
        firstNode.setAttribute(CONST_XML_UBL_EXT_NS, CONST_XML_EXT_VALUE);

        String documentType = firstNode.getLocalName();
        context.documentType(documentType);// first element should by documentType e.g. Invoice
        logger.debug("Document to be signed is of type {}", documentType);

        NodeList ublExtensions = document.getElementsByTagNameNS(CONST_XML_EXT_VALUE, CONST_EL_UBL_EXTENSIONS);
        Element ublExtensionsElement = findUblExtensionElement(document, firstNode, ublExtensions);

        createSignatureElement(document, ublExtensionsElement);
    }

    private void generateSignedPropertiesId(Context context) {
        String id = "id" + UUID.randomUUID().toString();

        logger.debug("generated signed property id of {}", id);

        context.signedPropertyId(id);
    }

    private void createSignatureElement(Document document, Element root) {
        logger.debug("creating signature information element");

        Element extensionNode = document.createElement(CONST_EL_UBL_EXTENSION);

        extensionNode.setAttribute(CONST_XML_SAC_NS, CONST_XML_SAC_VALUE);
        extensionNode.setAttribute(CONST_XML_SIG_NS, CONST_XML_SIG_VALUE);

        root.appendChild(extensionNode);

        Element content = document.createElement(CONST_EL_EXTENSION_CONTEXT);
        extensionNode.appendChild(content);

        Element signatures = document.createElement(CONST_EL_UBL_DOCUMENT_SIGNATURES);
        content.appendChild(signatures);

        Element signatureInformation = document.createElement(CONST_EL_SIGNATURE_INFORMATION);
        signatures.appendChild(signatureInformation);
    }

    private Element findUblExtensionElement(Document document, Node firstNode, NodeList ublExtensions) {
        if(ublExtensions.getLength() > 1 ) {
            throw new SignatureException("Found more then one UBLExtensions Element");
        } else if(ublExtensions.getLength() == 0) {
            logger.debug("found no UBLExtensions element. One will be created");
            return createUblExtensionsElement(document, firstNode);
        } else {
            logger.debug("found one UBLExtensions element.");
            return (Element) ublExtensions.item(0);
        }
    }

    private Element createUblExtensionsElement(Document document, Node firstNode) {
        logger.debug("creating element UBLExtensions");

        Element element = document.createElement("ext:UBLExtensions");

        firstNode.insertBefore(element, firstNode.getFirstChild());

        return element;
    }
}
