package hr.ingemark.edes.fina.sig.utils;

import hr.ingemark.edes.fina.sig.ex.SignatureException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;

public class XmlUtils {


    private XmlUtils() {
        // private constructor
    }

    // signature methods
    public static SignatureMethod getSha1SignatureMethod(XMLSignatureFactory signatureFactory) {

        try {
            return signatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new SignatureException("Cannot generate sha1 signature method", e);
        }
    }

    public static SignatureMethod getSha256SignatureMethod(XMLSignatureFactory signatureFactory) {

        try {
            return signatureFactory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new SignatureException("Cannot generate sha256 signature method", e);
        }
    }

    // canonization method

    public static CanonicalizationMethod getExclusiveWithCommentsCanonicalizationMethod(
            XMLSignatureFactory signatureFactory) {

        try {
            return signatureFactory
                    .newCanonicalizationMethod(
                            CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS, (C14NMethodParameterSpec) null);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new SignatureException("Cannot generate exclusive with comments canonicalization method", e);
        }
    }

    public static CanonicalizationMethod getInclusiveCanonicalizationMethod(
            XMLSignatureFactory signatureFactory) {

        try {
            return signatureFactory
                    .newCanonicalizationMethod(
                            CanonicalizationMethod.INCLUSIVE, (C14NMethodParameterSpec) null);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new SignatureException("Cannot generate exclusive with comments canonicalization method", e);
        }
    }

    // transforms
    public static Transform getXmlExcC14dTransform(XMLSignatureFactory signatureFactory) {

        try {
            return signatureFactory
                    .newTransform("http://www.w3.org/2001/10/xml-exc-c14n#WithComments", (TransformParameterSpec) null);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new SignatureException("Cannot generate ENVELOPED transform", e);
        }
    }

    public static Transform getEnvelopedTransform(XMLSignatureFactory signatureFactory) {

        try {
            return signatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new SignatureException("Cannot generate ENVELOPED transform", e);
        }
    }

    // digest methods

    public static DigestMethod getSha1DigestMethod(XMLSignatureFactory signatureFactory) {

        try {
            return signatureFactory.newDigestMethod(DigestMethod.SHA1, null);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new SignatureException("Cannot generate sha1 digest method", e);
        }
    }

    public static DigestMethod getSha256DigestMethod(XMLSignatureFactory signatureFactory) {

        try {
            return signatureFactory.newDigestMethod("http://www.w3.org/2001/04/xmlenc#sha256", null);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new SignatureException("Cannot generate sha1 digest method", e);
        }
    }

    // generics

    public static XMLSignatureFactory getXMLSignatureFactory() {
        String providerClassName = "org.jcp.xml.dsig.internal.dom.XMLDSigRI";

        Provider provider;

        try {
            provider = (Provider)Class.forName(providerClassName).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new SignatureException("Cannot create provider of class name " + providerClassName, e);
        }

        return XMLSignatureFactory.getInstance("DOM", provider);
    }

    public static byte[] writeDocumentToByteArray(Document document, TransformerFactory transformerFactory) {

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            Transformer transformer = transformerFactory.newTransformer();

            transformer.transform(new DOMSource(document), new StreamResult(baos));

            return baos.toByteArray();
        } catch (TransformerException | IOException e) {
            throw new SignatureException("cannot write document to byte array", e);
        }
    }

    public static Document loadXmlFromFile(String xml) {
        try {
            byte[] data = Files.readAllBytes(Paths.get(xml));

            return loadXmlFromBytes(data);
        } catch ( IOException e) {
            throw new SignatureException("Could not transform xml to Document", e);
        }
    }

    public static Document loadXmlFromBytes(byte[] data) {
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(data);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();

            return builder.parse(bais);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new SignatureException("Could not transform xml to Document", e);
        }
    }

    public static Document reloadDocument(Document document, TransformerFactory factory) {
        try {
            Transformer transformer = factory.newTransformer();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(document), new StreamResult(baos));

            return loadXmlFromBytes(baos.toByteArray());
        } catch ( TransformerException e) {
            throw new SignatureException("Cannot reload document", e);
        }
    }
}
