package hr.ingemark.edes.fina.sig.enums;

public enum NormType {

    HR_INVOICE,
    EN_16931,
    UBL_2_1; // generic UBL-2.1
}
