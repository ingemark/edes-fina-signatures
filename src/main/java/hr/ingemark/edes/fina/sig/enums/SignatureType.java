package hr.ingemark.edes.fina.sig.enums;

public enum SignatureType {

    XML_DSIG_2017,
    XML_DSIG_B2G,
    XML_XADES_B2G,
    XML_XADES
    ;
}
