package hr.ingemark.edes.fina.sig.utils;

import hr.ingemark.edes.fina.sig.ex.SignatureException;

import java.security.cert.X509Certificate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public final class FinaUtils {

    private FinaUtils() {

    }

    public static String extractCnInCertificate(X509Certificate cert) {
        String subjectDn = cert.getSubjectDN().getName();
        int indexOfCn = subjectDn.indexOf("CN");

        if(indexOfCn == -1) {
            throw new SignatureException("Cannot find CN in subject " + subjectDn);
        }

        int indexOfCnEnd = subjectDn.indexOf(',', indexOfCn);

        if(indexOfCnEnd == -1) {
            throw new SignatureException("Cannot find end of CN in subject " + subjectDn);
        }

        return subjectDn.substring(indexOfCn + 3, indexOfCnEnd);
    }

    public static String calculateAdobeTimestamp() {

        ZonedDateTime ts = ZonedDateTime.now();
        ZonedDateTime gmt = ts.withZoneSameInstant(ZoneId.of("GMT"));

        String offset = ts.getOffset().getId().replace(":", "\'") + "'";

        return "D:" + gmt.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + offset;
    }
}
