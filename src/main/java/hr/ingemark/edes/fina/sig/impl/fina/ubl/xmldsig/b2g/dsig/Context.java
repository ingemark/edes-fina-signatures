package hr.ingemark.edes.fina.sig.impl.fina.ubl.xmldsig.b2g.dsig;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.transform.TransformerFactory;
import java.security.cert.X509Certificate;

public class Context {

    private static final String SIGNATURE_ID = "data_signature";

    private Document document;

    private String signedPropertyId;

    private String documentType;

    private Element signatureInformationElement;

    private XMLObject adobeObject;

    private SignedInfo signedInfo;

    private XMLSignatureFactory signatureFactory;

    private TransformerFactory transformerFactory;

    private X509Certificate cert;

    private KeyInfo keyInfo;

    public Document document() {
        return this.document;
    }

    public String signedPropertyId() {
        return this.signedPropertyId;
    }

    public String documentType() {
        return this.documentType;
    }

    public Context document(final Document document) {
        this.document = document;
        return this;
    }

    public Context signedPropertyId(final String signedPropertyId) {
        this.signedPropertyId = signedPropertyId;
        return this;
    }

    public Context documentType(final String documentType) {
        this.documentType = documentType;
        return this;
    }

    public TransformerFactory transformerFactory() {
        return this.transformerFactory;
    }

    public Context transformerFactory(final TransformerFactory transformerFactory) {
        this.transformerFactory = transformerFactory;
        return this;
    }

    public Element signatureInformationElement() {
        return this.signatureInformationElement;
    }

    public Context signatureInformationElement(final Element signatureInformationElement) {
        this.signatureInformationElement = signatureInformationElement;
        return this;
    }

    public XMLSignatureFactory signatureFactory() {
        return this.signatureFactory;
    }

    public Context signatureFactory(final XMLSignatureFactory signatureFactory) {
        this.signatureFactory = signatureFactory;
        return this;
    }

    public X509Certificate cert() {
        return this.cert;
    }

    public Context cert(final X509Certificate cert) {
        this.cert = cert;
        return this;
    }

    public XMLObject adobeObject() {
        return this.adobeObject;
    }

    public Context adobeObject(final XMLObject adobeObject) {
        this.adobeObject = adobeObject;
        return this;
    }

    public SignedInfo signedInfo() {
        return this.signedInfo;
    }

    public Context signedInfo(final SignedInfo signedInfo) {
        this.signedInfo = signedInfo;
        return this;
    }

    public KeyInfo keyInfo() {
        return this.keyInfo;
    }

    public Context keyInfo(final KeyInfo keyInfo) {
        this.keyInfo = keyInfo;
        return this;
    }

    String signatureId() {
        return SIGNATURE_ID;
    }
}
