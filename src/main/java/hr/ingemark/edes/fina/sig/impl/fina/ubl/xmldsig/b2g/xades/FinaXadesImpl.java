package hr.ingemark.edes.fina.sig.impl.fina.ubl.xmldsig.b2g.xades;

import hr.ingemark.edes.fina.sig.config.KeystoreConfig;
import hr.ingemark.edes.fina.sig.ex.SignatureException;
import hr.ingemark.edes.fina.sig.utils.SignatureUtils;
import hr.ingemark.edes.fina.sig.interfaces.DigitalSigner;
import hr.ingemark.edes.fina.sig.utils.XmlUtils;
import hr.ingemark.edes.fina.sig.xml.DomUriDerefererencer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.keyinfo.X509IssuerSerial;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.crypto.dsig.spec.XPathFilterParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class FinaXadesImpl implements DigitalSigner {

    private static final Logger logger = LoggerFactory.getLogger(FinaXadesImpl.class);

    private static final String CONST_XML_EXT_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2";
    private static final String CONST_XML_CAC_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2";
    private static final String CONST_XML_SAC_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2";
    private static final String CONST_XML_SIG_VALUE = "urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2";

    private static final String CONST_XML_CAC_NAME = "cac";
    private static final String CONST_XML_EXT_NAME = "ext";
    private static final String CONST_XML_SAC_NS = "xmlns:sac";
    private static final String CONST_XML_SIG_NS = "xmlns:sig";
    private static final String CONST_XML_UBL_EXT_NS = "xmlns:ext";

    private static final String CONST_EL_UBL_EXTENSIONS = "UBLExtensions";
    private static final String CONST_EL_UBL_EXTENSION = "ext:UBLExtension";
    private static final String CONST_EL_EXTENSION_CONTEXT = "ext:ExtensionContent";
    private static final String CONST_EL_SIGNATURE_INFORMATION = "sac:SignatureInformation";
    private static final String CONST_EL_UBL_DOCUMENT_SIGNATURES = "sig:UBLDocumentSignatures";

    private KeystoreConfig keystoreConfig;

    public FinaXadesImpl(KeystoreConfig ksConf) {
        this.keystoreConfig = ksConf;
    }

    @Override
    public void sign(String sourceFilename, String targetFilename) {
        byte[] data = sign(sourceFilename);

        try {
            Files.write(Paths.get(targetFilename), data);
        } catch (IOException e) {
            throw new SignatureException("Cannot write document to file", e);
        }
    }

    @Override
    public byte[] sign(String filename) {
        try {
            return sign(Files.readAllBytes(Paths.get(filename)));
        } catch (IOException e) {
            throw new SignatureException("Cannot read file " + filename, e);
        }
    }

    @Override
    public byte[] sign(byte[] xmlContent) {
        Context context = new Context();

        loadXmlContent(context, xmlContent);
        signDocument(context);

        return convertToBytes(context);
    }

    private byte[] convertToBytes(Context context) {
        return XmlUtils.writeDocumentToByteArray(context.document(), context.transformerFactory());
    }

    private void loadXmlContent(Context context, byte[] xmlContent) {
        context.document(XmlUtils.loadXmlFromBytes(xmlContent));
    }

    private void signDocument(Context context) {

        try {
            generateTransformerFactory(context);
            generateSignatureFactory(context);
            loadCertificate(context);
            generateKeyInfo(context);

            addSignatureId(context);
            removeExistingSignature(context);
            addMissingElementsToXml(context);
            reloadXmlAfterModifications(context);

            selectSignatureInformationElement(context);

            createXadesElements(context);
            createSignedInfo(context);

            sign(context);
        } catch ( Exception e ) {
            throw new SignatureException("Cannot sign document", e);
        }
    }

    private void addSignatureId(Context context) {
        context.signatureId("Signature-" + UUID.randomUUID().toString().replace("-", ""));
    }

    private void sign(Context context) throws MarshalException, XMLSignatureException {
        logger.debug("signing document");

        DOMSignContext dsc = new DOMSignContext(this.keystoreConfig.getPrivateKey(), context.signatureInformationElement());
        dsc.setURIDereferencer(new DomUriDerefererencer());

        XMLSignature signature = context.signatureFactory()
                .newXMLSignature(
                        context.signedInfo(),
                        context.keyInfo(),
                        Collections.singletonList(context.xadesObject()),
                        context.signatureId(),
                        null
                );

        signature.sign(dsc);
    }

    private void createSignedInfo(Context context)
            throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {

        logger.debug("Creating signature info");

        CanonicalizationMethod canonicalizationMethod = XmlUtils
                .getInclusiveCanonicalizationMethod(context.signatureFactory());

        SignatureMethod signatureMethod = XmlUtils.getSha256SignatureMethod(context.signatureFactory());

        Map<String, String> namespaces = new HashMap<>();
        namespaces.put(CONST_XML_CAC_NAME, CONST_XML_CAC_VALUE);
        namespaces.put(CONST_XML_EXT_NAME, CONST_XML_EXT_VALUE);


        TransformParameterSpec transformDocumentSpec = new XPathFilterParameterSpec(
                "count(ancestor-or-self::sig:UBLDocumentSignatures | here()/ancestor::sig:UBLDocumentSignatures[1]) > count(ancestor-or-self::sig:UBLDocumentSignatures)",
                namespaces
        );

        Transform transformDocument = context.signatureFactory().newTransform(Transform.XPATH, transformDocumentSpec);

        List<Transform> transforms = Collections.singletonList(transformDocument);

        DigestMethod sha1DigestMethod = XmlUtils.getSha256DigestMethod(context.signatureFactory());

        Reference allDocumentReference = context.signatureFactory().newReference(
                "", sha1DigestMethod, transforms, null, null
        );

        Reference signedPropertiesReference = context.signatureFactory().newReference(
                "#" + context.signedPropertiesId(),
                sha1DigestMethod,
                Collections.emptyList(),
                "http://uri.etsi.org/01903#SignedProperties",
                null
        );

        context.signedInfo(context
                .signatureFactory()
                .newSignedInfo(
                        canonicalizationMethod,
                        signatureMethod,
                        Arrays.asList(allDocumentReference, signedPropertiesReference)
                )
        );
    }

    private void createXadesElements(Context context) throws NoSuchAlgorithmException, CertificateEncodingException, ParserConfigurationException {

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        Document xadesDocument = documentBuilderFactory.newDocumentBuilder().newDocument();

        // Object-> xades:QualifyingProperties
        Element qualifyingProperties = xadesDocument.createElement("xades:QualifyingProperties");
        qualifyingProperties.setAttribute("xmlns:xades", "http://uri.etsi.org/01903/v1.3.2#");
        qualifyingProperties.setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
        qualifyingProperties.setAttribute("Target", "#" + context.signatureId());

        // Object-> xades:QualifyingProperties -> xades:SignedProperties
        String signedPropertiesId = "SignedProperties-" + UUID.randomUUID().toString().replace("-", "");

        Element signedProperties = xadesDocument.createElement("xades:SignedProperties");
        signedProperties.setAttribute("Id", signedPropertiesId);
        signedProperties.setIdAttribute("Id", true);
        context.signedPropertiesId(signedPropertiesId);

        // Object-> xades:QualifyingProperties -> xades:SignedProperties -> xades:SignedSignatureProperties
        Element signedSignatureProperties = xadesDocument.createElement("xades:SignedSignatureProperties");

        // Object-> xades:QualifyingProperties -> xades:SignedProperties -> xades:SignedSignatureProperties
        // -> xades:SigningTime
        Element signingTime = xadesDocument.createElement("xades:SigningTime");
        signingTime.setTextContent(ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));

        // Object-> xades:QualifyingProperties -> xades:SignedProperties -> xades:SignedSignatureProperties
        // -> xades:SigningCertificate
        Element signingCertificate = xadesDocument.createElement("xades:SigningCertificate");
        Element cert = xadesDocument.createElement("xades:Cert");
        Element certDigest = xadesDocument.createElement("xades:CertDigest");
        Element certDigestMethod = xadesDocument.createElement("ds:DigestMethod");
        certDigestMethod.setAttribute("Algorithm", DigestMethod.SHA1);

        MessageDigest sha1MessageDigest = MessageDigest.getInstance("SHA1");
        Element certDigestValue = xadesDocument.createElement("ds:DigestValue");
        certDigestValue.setTextContent(Base64.getEncoder().encodeToString(sha1MessageDigest.digest(context.cert().getEncoded())));

        certDigest.appendChild(certDigestMethod);
        certDigest.appendChild(certDigestValue);

        Element certIssuerSerial = xadesDocument.createElement("xades:IssuerSerial");

        Element certX509IssuerName = xadesDocument.createElement("ds:X509IssuerName");
        certX509IssuerName.setTextContent(context.cert().getIssuerX500Principal().getName());

        Element certX509SerialNumber = xadesDocument.createElement("ds:X509SerialNumber");
        certX509SerialNumber.setTextContent(context.cert().getSerialNumber().toString());

        certIssuerSerial.appendChild(certX509IssuerName);
        certIssuerSerial.appendChild(certX509SerialNumber);

        cert.appendChild(certDigest);
        cert.appendChild(certIssuerSerial);
        signingCertificate.appendChild(cert);

        // Object-> xades:QualifyingProperties -> xades:SignedProperties -> xades:SignedDataObjectProperties
        Element signedDataObjectProperties = xadesDocument.createElement("xades:SignedDataObjectProperties");

        // Object-> xades:QualifyingProperties -> xades:SignedProperties -> xades:SignedDataObjectProperties
        // -> xades:DataObjectFormat
        Element dataObjectFormat = xadesDocument.createElement("xades:DataObjectFormat");
        dataObjectFormat.setAttribute("ObjectReference", "");

        Element dataObjectFormatDescription = xadesDocument.createElement("xades:Description");
        dataObjectFormatDescription.setTextContent("document");

        Element dataObjectFormatMimeType = xadesDocument.createElement("xades:MimeType");
        dataObjectFormatMimeType.setTextContent("application/xml");

        Element dataObjectFormatEncoding = xadesDocument.createElement("xades:Encoding");
        dataObjectFormatEncoding.setTextContent("UTF-8");

        dataObjectFormat.appendChild(dataObjectFormatDescription);
        dataObjectFormat.appendChild(dataObjectFormatMimeType);
        dataObjectFormat.appendChild(dataObjectFormatEncoding);

        signedDataObjectProperties.appendChild(dataObjectFormat);

        signedSignatureProperties.appendChild(signingTime);
        signedSignatureProperties.appendChild(signingCertificate);

        signedProperties.appendChild(signedSignatureProperties);
        signedProperties.appendChild(signedDataObjectProperties);

        qualifyingProperties.appendChild(signedProperties);

        xadesDocument.appendChild(qualifyingProperties);

        // must reload to void issues with signing process
        xadesDocument = XmlUtils.reloadDocument(xadesDocument, context.transformerFactory());

        DOMStructure xadesStructure = new DOMStructure(xadesDocument.getFirstChild());

        XMLObject xadesObject = context.signatureFactory().newXMLObject(
                Collections.singletonList(xadesStructure), null, null, null);

        context.xadesObject(xadesObject);
    }

    private void selectSignatureInformationElement(Context context) {
        NodeList nodeList = context.document().getElementsByTagName(CONST_EL_SIGNATURE_INFORMATION);

        Element signatureInformationElement = (Element) nodeList.item(nodeList.getLength() - 1);

        context.signatureInformationElement(signatureInformationElement);
    }

    private void reloadXmlAfterModifications(Context context) {
        logger.debug("Reloading document with modifications");
        // this step is mandatory since without it signature will be invalid
        // not sure what the cause it.
        // It probably has to to with the fact how a transformer works and that we are changing the original
        // document in order to add SignatureInformation
        convertBytesToDocument(context, convertDocumentToBytes(context));
    }

    private void convertBytesToDocument(Context context, byte[] content) {
        context.document(XmlUtils.loadXmlFromBytes(content));
    }

    private byte[] convertDocumentToBytes(Context context) {
        try {
            Transformer transformer = context.transformerFactory().newTransformer();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(context.document()), new StreamResult(baos));

            // clear document so its eligible for gc
            context.document(null);
            return baos.toByteArray();
        } catch (TransformerException e ) {
            throw new SignatureException("Cannot convert document to bytes", e);
        }
    }

    private void removeExistingSignature(Context context) {
        Document document = context.document();

        NodeList signatures = document.getElementsByTagNameNS(CONST_XML_SIG_VALUE, "UBLDocumentSignatures");

        if (signatures.getLength() > 0) {
            Node parentNode = signatures.item(0).getParentNode().getParentNode();
            parentNode.getParentNode().removeChild(parentNode);
        }
    }

    private void addMissingElementsToXml(Context context) {
        Document document = context.document();

        Element firstNode = (Element) document.getFirstChild();
        firstNode.setAttribute(CONST_XML_UBL_EXT_NS, CONST_XML_EXT_VALUE);

        String documentType = firstNode.getLocalName();
        context.documentType(documentType);// first element should by documentType e.g. Invoice
        logger.debug("Document to be signed is of type {}", documentType);

        NodeList ublExtensions = document.getElementsByTagNameNS(CONST_XML_EXT_VALUE, CONST_EL_UBL_EXTENSIONS);
        Element ublExtensionsElement = findUblExtensionElement(document, firstNode, ublExtensions);

        createSignatureElement(document, ublExtensionsElement);
    }

    private void createSignatureElement(Document document, Element root) {
        logger.debug("creating signature information element");

        Element extensionNode = document.createElement(CONST_EL_UBL_EXTENSION);

        extensionNode.setAttribute(CONST_XML_SAC_NS, CONST_XML_SAC_VALUE);
        extensionNode.setAttribute(CONST_XML_SIG_NS, CONST_XML_SIG_VALUE);

        root.appendChild(extensionNode);

        Element content = document.createElement(CONST_EL_EXTENSION_CONTEXT);
        extensionNode.appendChild(content);

        Element signatures = document.createElement(CONST_EL_UBL_DOCUMENT_SIGNATURES);
        content.appendChild(signatures);

        Element signatureInformation = document.createElement(CONST_EL_SIGNATURE_INFORMATION);
        signatures.appendChild(signatureInformation);
    }

    private Element findUblExtensionElement(Document document, Node firstNode, NodeList ublExtensions) {
        if(ublExtensions.getLength() > 1 ) {
            throw new SignatureException("Found more then one UBLExtensions Element");
        } else if(ublExtensions.getLength() == 0) {
            logger.debug("found no UBLExtensions element. One will be created");
            return createUblExtensionsElement(document, firstNode);
        } else {
            logger.debug("found one UBLExtensions element.");
            return (Element) ublExtensions.item(0);
        }
    }

    private Element createUblExtensionsElement(Document document, Node firstNode) {
        logger.debug("creating element UBLExtensions");

        Element element = document.createElement("ext:UBLExtensions");

        firstNode.insertBefore(element, firstNode.getFirstChild());

        return element;
    }

    private void generateKeyInfo(Context context) throws KeyException {
        logger.debug("Generating key info from provided keystore");

        X509Certificate cert = context.cert();
        KeyInfoFactory keyInfoFactory = context.signatureFactory().getKeyInfoFactory();

        KeyValue keyValue = keyInfoFactory.newKeyValue(cert.getPublicKey());
        X509IssuerSerial serial = keyInfoFactory
                .newX509IssuerSerial(cert.getIssuerX500Principal().getName(), cert.getSerialNumber());

        byte[] ski = cert.getExtensionValue("2.5.29.14");
        List<Object> x509DataContent = Arrays.asList(serial, cert.getSubjectDN().getName(), ski, cert);

        X509Data x509Data = keyInfoFactory.newX509Data(x509DataContent);

        context.keyInfo(keyInfoFactory.newKeyInfo(Arrays.asList(keyValue, x509Data)));
    }

    private void loadCertificate(Context context) {
        context.cert(this.keystoreConfig.getCertificate());
    }

    private void generateSignatureFactory(Context context) {
        logger.debug("Generating new xml signature factory");

        context.signatureFactory(XmlUtils.getXMLSignatureFactory());
    }

    private void generateTransformerFactory(Context context) {
        logger.debug("generating new transformer factory");

        context.transformerFactory(SignatureUtils.buildTransformerFactory());
    }
}