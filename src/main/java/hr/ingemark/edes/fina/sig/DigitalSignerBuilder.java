package hr.ingemark.edes.fina.sig;

import hr.ingemark.edes.fina.sig.config.KeystoreConfig;
import hr.ingemark.edes.fina.sig.enums.NormType;
import hr.ingemark.edes.fina.sig.enums.SignatureType;
import hr.ingemark.edes.fina.sig.ex.SignatureException;
import hr.ingemark.edes.fina.sig.impl.fina.ubl.xmldsig.b2g.dsig.FinaDsigB2gImpl;
import hr.ingemark.edes.fina.sig.impl.fina.ubl.xmldsig.b2g.xades.FinaXadesImpl;
import hr.ingemark.edes.fina.sig.impl.fina.ubl.xmldsig.y2017.FinaDsigSignatureServiceImpl;
import hr.ingemark.edes.fina.sig.interfaces.DigitalSigner;
import hr.ingemark.edes.fina.sig.utils.KeyStoreUtils;

import java.security.Key;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Objects;

public class DigitalSignerBuilder {

    private KeystoreConfig keystoreConfig;

    private NormType normType;

    private SignatureType signatureType;

    private DigitalSignerBuilder() {

    }

    public static DigitalSignerBuilder builder() {
        return new DigitalSignerBuilder();
    }

    public KeystoreBuilder withKeystore() {
        return new KeystoreBuilder(this);
    }

    public ExtractedKeystoreBuilder withExtractedKeystore() {
        return new ExtractedKeystoreBuilder(this);
    }

    public DigitalSignerBuilder withNormType(NormType normType) {
        Objects.requireNonNull(normType);

        this.normType = normType;
        return this;
    }

    public DigitalSignerBuilder withSignatureType(SignatureType signatureType) {
        Objects.requireNonNull(signatureType);

        this.signatureType = signatureType;
        return this;
    }

    public DigitalSigner build() {
        Objects.requireNonNull(keystoreConfig);
        Objects.requireNonNull(normType);
        Objects.requireNonNull(signatureType);

        if (normType == NormType.HR_INVOICE && signatureType == SignatureType.XML_DSIG_2017) {
            return new FinaDsigSignatureServiceImpl(this.keystoreConfig);
        } else if (normType == NormType.HR_INVOICE && signatureType == SignatureType.XML_DSIG_B2G) {
            return new FinaDsigB2gImpl(this.keystoreConfig);
        } else if (normType == NormType.EN_16931 && signatureType == SignatureType.XML_XADES_B2G) {
            return new FinaXadesImpl(this.keystoreConfig);
        } else if (normType == NormType.EN_16931 && signatureType == SignatureType.XML_XADES) {
            return new FinaXadesImpl(this.keystoreConfig);
        } else if (normType == NormType.UBL_2_1 && signatureType == SignatureType.XML_XADES) {
            return new FinaXadesImpl(this.keystoreConfig);
        } else {
            throw new SignatureException("not implemented for norm " + normType + " and signature " + signatureType);
        }
    }

    public class KeystoreBuilder {

        private String filename;

        private String password;

        private String alias;

        private DigitalSignerBuilder builder;

        private KeystoreBuilder(DigitalSignerBuilder builder) {
            this.builder = builder;
        }

        public KeystoreBuilder withFilename(String filename) {
            Objects.requireNonNull(filename);

            this.filename = filename;
            return this;
        }

        public KeystoreBuilder withPassword(String password) {
            Objects.requireNonNull(password);

            this.password = password;
            return this;
        }

        public KeystoreBuilder withAlias(String alias) {
            Objects.requireNonNull(alias);

            this.alias = alias;
            return this;
        }

        public DigitalSignerBuilder build() {
            KeyStore keyStore = KeyStoreUtils.loadJksKeyStore(filename, password);
            X509Certificate certificate = KeyStoreUtils.getCertificate(keyStore, alias);

            Key privateKey = KeyStoreUtils.loadPrivateKey(keyStore, alias, password);

            builder.keystoreConfig = new KeystoreConfig(certificate, privateKey);
            return builder;
        }
    }

    public class ExtractedKeystoreBuilder {
        private X509Certificate certificate;

        private Key privateKey;

        private DigitalSignerBuilder builder;

        private ExtractedKeystoreBuilder(DigitalSignerBuilder builder) {
            this.builder = builder;
        }

        public ExtractedKeystoreBuilder withCertificate(X509Certificate certificate) {
            Objects.requireNonNull(certificate);

            this.certificate = certificate;
            return this;
        }

        public ExtractedKeystoreBuilder withPrivateKey(Key privateKey) {
            Objects.requireNonNull(privateKey);

            this.privateKey = privateKey;
            return this;
        }

        public DigitalSignerBuilder build() {
            builder.keystoreConfig = new KeystoreConfig(certificate, privateKey);
            return builder;
        }
    }
}
