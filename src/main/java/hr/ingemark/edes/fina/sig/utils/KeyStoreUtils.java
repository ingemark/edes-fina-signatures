package hr.ingemark.edes.fina.sig.utils;

import hr.ingemark.edes.fina.sig.ex.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;

public class KeyStoreUtils {

    private static final Logger logger = LoggerFactory.getLogger(KeyStoreUtils.class);

    private static final String KEY_STORE_INSTANCE_TYPE = "JKS";

    private KeyStoreUtils() {
        // utils
    }

    public static KeyStore loadJksKeyStore(String keystoreLocation, String keystorePassword) {
        try (InputStream inputStream = new FileInputStream(keystoreLocation)) {

            KeyStore jksKeystore = KeyStore.getInstance(KEY_STORE_INSTANCE_TYPE);

            jksKeystore.load(inputStream, keystorePassword.toCharArray());

            return jksKeystore;
        } catch (Exception e) {
            logger.error("Cannot load key store", e);
            throw new SignatureException("Cannot load KeyStore", e);
        }
    }

    public static Key loadPrivateKey(KeyStore keyStore, String alias, String aliasPassword) {
        try {
            return keyStore.getKey(alias, aliasPassword.toCharArray());
        } catch (Exception e) {
            logger.error("Cannot load private key", e);
            throw new SignatureException("Cannot load PrivateKey", e);
        }
    }

    public static X509Certificate getCertificate(KeyStore keyStore, String alias) {
        try {
            return (X509Certificate) keyStore.getCertificate(alias);
        } catch (KeyStoreException e) {
            throw new SignatureException("cannot fetch certificate by alias", e);
        }
    }
}
