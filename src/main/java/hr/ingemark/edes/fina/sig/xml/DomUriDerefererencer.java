package hr.ingemark.edes.fina.sig.xml;

import org.apache.jcp.xml.dsig.internal.dom.ApacheNodeSetData;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.crypto.Data;
import javax.xml.crypto.URIDereferencer;
import javax.xml.crypto.URIReference;
import javax.xml.crypto.URIReferenceException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dom.DOMURIReference;

/**
 * This class uses propriety Sun API. Should be changed when some other implementation is found
 */
public class DomUriDerefererencer implements URIDereferencer {

    private static final String CONST_SIGNED_PROPERTIES = "http://uri.etsi.org/01903#SignedProperties";
    private static final String CONST_NS_XADES = "http://uri.etsi.org/01903/v1.3.2#";
    private static final String CONST_EL_SIGNED_PROPERTIES = "SignedProperties";

    @Override
    public Data dereference(URIReference uriReference, XMLCryptoContext xmlCryptoContext) throws URIReferenceException {
        DOMURIReference domReference = (DOMURIReference)uriReference;

        if(domReference.getURI() == null || domReference.getURI().equals("")) {
            return new ApacheNodeSetData(new XMLSignatureInput(domReference.getHere().getOwnerDocument()));
        }

        if(domReference.getType() != null && domReference.getType().equals(CONST_SIGNED_PROPERTIES)) {
            String id = domReference.getURI().substring(1);

            NodeList nodeList = domReference
                    .getHere()
                    .getOwnerDocument()
                    .getElementsByTagNameNS(CONST_NS_XADES, CONST_EL_SIGNED_PROPERTIES);

            if(nodeList.getLength() == 0) {
                throw new URIReferenceException("Cannot find SignedProperties");
            }

            for(int i = 0; i < nodeList.getLength(); i++) {
                Element node = (Element) nodeList.item(0);

                String idAttribute = node.getAttribute("Id");
                if(idAttribute == null || idAttribute.equals(id)) {
                    return new ApacheNodeSetData(new XMLSignatureInput(node));
                }
            }

        }

        throw new URIReferenceException("cannot find reference of type " + uriReference.getType());
    }
}
