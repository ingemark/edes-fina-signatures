package hr.ingemark.edes.fina.sig.config;

import java.security.Key;
import java.security.cert.X509Certificate;
import java.util.Objects;

public class KeystoreConfig {

    private final X509Certificate certificate;

    private final Key privateKey;

    public KeystoreConfig(final X509Certificate certificate, final Key privateKey) {

        Objects.requireNonNull(certificate);
        Objects.requireNonNull(privateKey);

        this.certificate = certificate;
        this.privateKey = privateKey;
    }

    public X509Certificate getCertificate() {
        return certificate;
    }

    public Key getPrivateKey() {
        return privateKey;
    }
}
