package hr.ingemark.edes.fina.sig.utils;

import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class X509KeySelector extends KeySelector {

    private static final String X509_CERTIFICATE = "X509";

    private static final String PKIX = "PKIX";

    private KeyStore keyStore;

    public X509KeySelector(KeyStore keyStore) {
        this.keyStore = keyStore;
    }

    public KeySelectorResult select(
            KeyInfo keyInfo, Purpose purpose, AlgorithmMethod method, XMLCryptoContext context)
            throws KeySelectorException {

        List<X509Certificate> certificates = new ArrayList<>();
        for (Object keyInfoContent : keyInfo.getContent()) {
            XMLStructure info = (XMLStructure) keyInfoContent;
            if (!(info instanceof X509Data))
                continue;

            X509Data x509Data = (X509Data) info;
            for (Object x509DataContent : x509Data.getContent()) {
                if (!(x509DataContent instanceof X509Certificate))
                    continue;

                final X509Certificate x509Certificate = (X509Certificate) x509DataContent;
                certificates.add(x509Certificate);
            }
        }

        if (certificates.isEmpty()) {
            throw new KeySelectorException("Missing X509Certificate");
        }

        try {
            final CertificateFactory certificateFactory = CertificateFactory.getInstance(X509_CERTIFICATE);
            final CertPath certPath = certificateFactory.generateCertPath(certificates);
            final CertPathValidator pathValidator = CertPathValidator.getInstance(PKIX);

            final PKIXParameters pkixParameters = new PKIXParameters(keyStore);
            pkixParameters.setRevocationEnabled(false);
            pathValidator.validate(certPath, pkixParameters);

            final PublicKey key = certificates.get(0).getPublicKey();
            return () -> key;
        } catch (CertificateException | NoSuchAlgorithmException | CertPathValidatorException
                | InvalidAlgorithmParameterException | KeyStoreException e) {
            throw new KeySelectorException(e);
        }
    }
}
