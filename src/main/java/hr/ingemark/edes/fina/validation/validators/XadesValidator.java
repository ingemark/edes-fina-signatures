package hr.ingemark.edes.fina.validation.validators;

import java.io.ByteArrayInputStream;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.dom.DOMValidateContext;

import hr.ingemark.edes.fina.sig.utils.X509KeySelector;
import hr.ingemark.edes.fina.sig.utils.XmlUtils;
import hr.ingemark.edes.fina.sig.xml.DomUriDerefererencer;
import hr.ingemark.edes.fina.validation.SignatureValidationReport;
import hr.ingemark.edes.fina.validation.utils.DOMUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XadesValidator {

  private static final Logger logger = LoggerFactory.getLogger(XadesValidator.class);

  private static final String CORRECT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

  public static final String CONST_NS_XMLDSIG = "http://www.w3.org/2000/09/xmldsig#";
  public static final String CONST_NS_XADES = "http://uri.etsi.org/01903/v1.3.2#";

  private static final String CONST_EL_CANONICALIZATION_METHOD = "CanonicalizationMethod";

  private static final String CONST_EL_SIGNATURE_METHOD = "SignatureMethod";
  private static final String CONST_ATTR_SIGNATURE_METHOD = "Algorithm";
  private static final String CONST_ATTR_SIGNATURE_METHOD_VALUE =
      "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

  private static final String CONST_EL_REFERENCE = "Reference";
  private static final String CONST_ATTR_REFERENCE_URI = "URI";
  private static final String CONST_ATTR_REFERENCE_URI_VALUE_1 = "";

  private static final String CONST_ATTR_REFERENCE_TYPE = "Type";
  private static final String CONST_ATTR_REFERENCE_TYPE_VALUE =
      "http://uri.etsi.org/01903#SignedProperties";

  private static final String CONST_EL_XPATH = "XPath";

  private static final String CONST_EL_KEY_INFO = "KeyInfo";

  private static final String CONST_EL_KEY_VALUE = "KeyValue";
  private static final String CONST_EL_RSAKEYVALUE = "RSAKeyValue";
  private static final String CONST_EL_MODULUS = "Modulus";
  private static final String CONST_EL_EXPONENT = "Exponent";

  private static final String CONST_EL_X509_DATA = "X509Data";
  private static final String CONST_EL_X509_SUBJECT_NAME = "X509SubjectName";
  private static final String CONST_EL_X509_CERTIFICATE = "X509Certificate";

  private static final String CONST_EL_QUALIFYING_PROPERTIES = "SignedProperties";
  private static final String CONST_EL_SIGNING_TIME = "SigningTime";
  private static final String CONST_EL_CERT_DIGEST_VALUE = "DigestValue";
  private static final String CONST_EL_CERT_DIGEST_METHOD = "DigestMethod";

  private static final String CONST_EL_DIGEST_METHOD = "DigestMethod";
  private static final String CONST_EL_DIGEST_VALUE = "DigestValue";
  private static final String CONST_ATTR_DIGEST_METHOD_ALGORITHM = "Algorithm";

  private static final List<String> CONST_ATTR_DIGEST_METHOD_VALUES =
      Collections.unmodifiableList(
          Arrays.asList(
              "http://www.w3.org/2000/09/xmldsig#sha1",
              "http://www.w3.org/2001/04/xmlenc#sha256",
              "http://www.w3.org/2001/04/xmlenc#sha512",
              "http://www.w3.org/2001/04/xmlenc#ripemd160"));

  private static final String CONST_EL_SIGNATURE = "Signature";
  private static final String CONST_EL_SIGNATURE_VALUE = "SignatureValue";

  private static final String ELEMENT_NOT_FOUND_MESSAGE = "Element not found.";

  private KeyStore keystore;

  public XadesValidator(KeyStore keystore) {
    this.keystore = keystore;
  }

  public SignatureValidationReport validate(Document document) {

    List<Report> reports = new ArrayList<>();

    validateCanonicalizationMethod(document, reports);
    validateSignatureMethod(document, reports);
    validateReference1(document, reports);
    validateReference2(document, reports);
    validateKeyInfo(document, reports);
    validateQualifyingProperties(document, reports);

    // validate signature only if everything else is valid
    boolean currentValidationReportStatus =
        reports.stream().map(Report::isValid).reduce(Boolean::logicalAnd).orElse(true);

    if (currentValidationReportStatus) {
      validateDocumentSignature(document, reports);
    }

    return new SignatureValidationReport(reports);
  }

  private void validateDocumentSignature(Document document, List<Report> reports) {
    logger.debug("Validating document signature");

    try {
      NodeList signatureNodeList =
          document.getElementsByTagNameNS(XMLSignature.XMLNS, CONST_EL_SIGNATURE);
      Node signatureNode = signatureNodeList.item(0);

      DOMValidateContext validateContext =
          new DOMValidateContext(new X509KeySelector(this.keystore), signatureNode);
      validateContext.setURIDereferencer(new DomUriDerefererencer());

      XMLSignature signature =
          XmlUtils.getXMLSignatureFactory().unmarshalXMLSignature(validateContext);

      if (!signature.validate(validateContext)) {
        reports.add(new Report(false, CONST_EL_SIGNATURE_VALUE, "Document signature is invalid"));
      }
    } catch (MarshalException | XMLSignatureException e) {
      logger.trace("Cannot validate document signature", e);
      reports.add(new Report(false, CONST_EL_SIGNATURE_VALUE, e.toString()));
    }
  }

  private void validateCanonicalizationMethod(Document document, List<Report> reports) {

    logger.debug("Validating canonicalization method");

    Optional<Element> elementOptional =
        DOMUtils.findExactlyOneElement(
            CONST_NS_XMLDSIG, CONST_EL_CANONICALIZATION_METHOD, document);
    if (!elementOptional.isPresent()) {
      reportNotFound(reports, CONST_EL_CANONICALIZATION_METHOD, ELEMENT_NOT_FOUND_MESSAGE);
    }
  }

  private void validateSignatureMethod(Document document, List<Report> reports) {

    logger.debug("Validating signature method");

    Optional<Element> elementOptional =
        DOMUtils.findExactlyOneElement(CONST_NS_XMLDSIG, CONST_EL_SIGNATURE_METHOD, document);
    if (!elementOptional.isPresent()) {
      reportNotFound(reports, CONST_EL_SIGNATURE_METHOD, ELEMENT_NOT_FOUND_MESSAGE);
      return;
    }
    validateElementAttributeValue(
        elementOptional.get(),
        CONST_EL_SIGNATURE_METHOD,
        CONST_ATTR_SIGNATURE_METHOD,
        CONST_ATTR_SIGNATURE_METHOD_VALUE,
        reports);
  }

  private void validateReference1(Document document, List<Report> reports) {

    logger.debug("Validating reference");

    Optional<Element> referenceElOpt =
        DOMUtils.findElementByTagNameAndAttribute(
            CONST_NS_XMLDSIG,
            CONST_EL_REFERENCE,
            CONST_ATTR_REFERENCE_URI,
            CONST_ATTR_REFERENCE_URI_VALUE_1,
            document);

    if (!referenceElOpt.isPresent()) {
      reportNotFound(
          reports,
          CONST_EL_REFERENCE,
          String.format(
              "Element %s with attribute %s = %s not found",
              CONST_EL_REFERENCE, CONST_ATTR_REFERENCE_URI, CONST_ATTR_REFERENCE_URI_VALUE_1));
      return;
    }
    validateElementAttributeValue(
        referenceElOpt.get(),
        CONST_EL_REFERENCE,
        CONST_ATTR_REFERENCE_URI,
        CONST_ATTR_REFERENCE_URI_VALUE_1,
        reports);

    validateReferenceTransformationXpath(referenceElOpt.get(), reports);
    validateReferenceDigestMethod(referenceElOpt.get(), reports);
    validateReferenceDigestValue(referenceElOpt.get(), reports);
  }

  private void validateReferenceDigestMethod(Element element, List<Report> reports) {

    logger.debug("Validating reference - DigestMethod");

    Optional<Element> digestMethodElOpt =
        DOMUtils.findExactlyOneElement(CONST_NS_XMLDSIG, CONST_EL_DIGEST_METHOD, element);
    if (!digestMethodElOpt.isPresent()) {
      reportNotFound(
          reports,
          CONST_EL_REFERENCE,
          String.format("%s element not found ", CONST_EL_DIGEST_METHOD));
      return;
    }
    validateElementAttributeValue(digestMethodElOpt.get(), reports);
  }

  private void validateReferenceDigestValue(Element element, List<Report> reports) {
    logger.debug("Validating reference - DigestValue");
    validateElementValueExist(CONST_NS_XMLDSIG, CONST_EL_DIGEST_VALUE, element, reports);
  }

  private void validateReferenceTransformationXpath(Element reference, List<Report> reports) {

    logger.debug("Validating reference - XPath");

    Optional<Element> xPathElOpt =
        DOMUtils.findExactlyOneElement(CONST_NS_XMLDSIG, CONST_EL_XPATH, reference);
    if (!xPathElOpt.isPresent()) {
      reportNotFound(
          reports, CONST_EL_REFERENCE, String.format("%s element not found", CONST_EL_XPATH));
    }
  }

  private void validateReference2(Document document, List<Report> reports) {

    logger.debug("Validating reference");

    Optional<Element> referenceElOpt =
        DOMUtils.findElementByTagNameAndAttribute(
            CONST_NS_XMLDSIG,
            CONST_EL_REFERENCE,
            CONST_ATTR_REFERENCE_TYPE,
            CONST_ATTR_REFERENCE_TYPE_VALUE,
            document);

    if (!referenceElOpt.isPresent()) {
      reportNotFound(
          reports,
          CONST_EL_REFERENCE,
          String.format(
              "%s element with attribute %s = %s not found",
              CONST_EL_REFERENCE, CONST_ATTR_REFERENCE_TYPE, CONST_ATTR_REFERENCE_TYPE_VALUE));
      return;
    }
    validateElementAttributeExist(
        referenceElOpt.get(), CONST_EL_REFERENCE, CONST_ATTR_REFERENCE_URI, reports);

    validateElementAttributeValue(
        referenceElOpt.get(),
        CONST_EL_REFERENCE,
        CONST_ATTR_REFERENCE_TYPE,
        CONST_ATTR_REFERENCE_TYPE_VALUE,
        reports);

    validateReferenceDigestMethod(referenceElOpt.get(), reports);
    validateReferenceDigestValue(referenceElOpt.get(), reports);
  }

  private void validateKeyInfo(Document document, List<Report> reports) {

    logger.debug("Validating keyInfo");

    Optional<Element> keyInfoElOpt =
        DOMUtils.findExactlyOneElement(CONST_NS_XMLDSIG, CONST_EL_KEY_INFO, document);
    if (!keyInfoElOpt.isPresent()) {
      reportNotFound(reports, CONST_EL_KEY_INFO, ELEMENT_NOT_FOUND_MESSAGE);
      return;
    }

    Optional<Element> x509DataElOpt =
        DOMUtils.findExactlyOneElement(CONST_NS_XMLDSIG, CONST_EL_X509_DATA, keyInfoElOpt.get());
    if (x509DataElOpt.isPresent()) {
      validateX509Data(x509DataElOpt.get(), reports);
    } else {
      reportNotFound(reports, CONST_EL_X509_DATA, ELEMENT_NOT_FOUND_MESSAGE);
    }
  }

  private void validateX509Data(Element x509Data, List<Report> reports) {

    logger.debug("Validating keyInfo - X509Data");

    validateElementValueExist(CONST_NS_XMLDSIG, CONST_EL_X509_SUBJECT_NAME, x509Data, reports);
    validateElementValueExist(CONST_NS_XMLDSIG, CONST_EL_X509_CERTIFICATE, x509Data, reports);
  }

  private void validateQualifyingProperties(Document document, List<Report> reports) {

    logger.debug("Validating qualifyingProperties");

    Optional<Element> qualifyingPropertiesElOpt =
        DOMUtils.findExactlyOneElement(CONST_NS_XADES, CONST_EL_QUALIFYING_PROPERTIES, document);

    if (!qualifyingPropertiesElOpt.isPresent()) {
      reportNotFound(reports, CONST_EL_QUALIFYING_PROPERTIES, ELEMENT_NOT_FOUND_MESSAGE);
      return;
    }
    validateSigningTime(qualifyingPropertiesElOpt.get(), reports);
    validateSigningCertificateDigestValue(qualifyingPropertiesElOpt.get(), reports, document);
  }

  private void validateSigningTime(Element qualifyingProperties, List<Report> reports) {

    logger.debug("Validating qualifyingProperties - signingTime");

    Optional<Element> signingTimeElOpt =
        DOMUtils.findExactlyOneElement(CONST_NS_XADES, CONST_EL_SIGNING_TIME, qualifyingProperties);

    if (!signingTimeElOpt.isPresent()) {
      reportNotFound(reports, CONST_EL_SIGNING_TIME, ELEMENT_NOT_FOUND_MESSAGE);
      return;
    }

    String time = signingTimeElOpt.get().getTextContent();
    try {
      if (!(isDateInCorrectFormat(time))) {
        reports.add(
            new Report(
                false,
                CONST_EL_SIGNING_TIME,
                String.format(
                    "Incorrect signing time, must be in format %s", CORRECT_DATE_FORMAT)));
      }
    } catch (DateTimeParseException e) {
      reports.add(new Report(false, CONST_EL_SIGNING_TIME, e.getMessage()));
    }
  }

  private void validateSigningCertificateDigestValue(
      Element qualifyingProperties, List<Report> reports, Document document) {

    logger.debug("Validating qualifyingProperties - signingCertificateDigestValue");

    Optional<Element> elementX509CertificateOptional =
        DOMUtils.findExactlyOneElement(CONST_NS_XMLDSIG, CONST_EL_X509_CERTIFICATE, document);
    if (!elementX509CertificateOptional.isPresent()) {
      reportNotFound(
          reports,
          CONST_EL_CERT_DIGEST_VALUE,
          "Element X509Certificate not found - cannot validate signing certificate digest value");
      return;
    }

    if (elementX509CertificateOptional.get().getTextContent().trim().isEmpty()) {
      reportNotFound(
          reports,
          CONST_EL_CERT_DIGEST_VALUE,
          "Element X509Certificate is empty - cannot validate signing certificate digest value");
      return;
    }

    String base64encodedX509Certificate =
        elementX509CertificateOptional.get().getTextContent().replaceAll("\\s+", "").trim();

    logger.debug("base64encodedX509Certificate: {}", base64encodedX509Certificate);

    Optional<Element> digestMethodOpt =
            DOMUtils.findExactlyOneElement(CONST_NS_XMLDSIG, CONST_EL_CERT_DIGEST_METHOD, qualifyingProperties);
    if (!digestMethodOpt.isPresent()) {
      reportNotFound(reports, CONST_EL_CERT_DIGEST_METHOD, ELEMENT_NOT_FOUND_MESSAGE);
      return;
    }
    validateElementAttributeValue(digestMethodOpt.get(), reports);

    String digestMethod = digestMethodOpt.get().getAttribute(CONST_ATTR_DIGEST_METHOD_ALGORITHM );

    Optional<Element> digestValueElOpt =
        DOMUtils.findExactlyOneElement(
            CONST_NS_XMLDSIG, CONST_EL_CERT_DIGEST_VALUE, qualifyingProperties);
    if (!digestValueElOpt.isPresent()) {
      reportNotFound(reports, CONST_EL_CERT_DIGEST_VALUE, ELEMENT_NOT_FOUND_MESSAGE);
      return;
    }
    String digestValue = digestValueElOpt.get().getTextContent();

    logger.debug("digestValue: {}", digestValue);
    try {

      CertificateFactory fac = CertificateFactory.getInstance("X509");

      byte[] encodedCert = Base64.getDecoder().decode(base64encodedX509Certificate);
      X509Certificate cert =
          (X509Certificate) fac.generateCertificate(new ByteArrayInputStream(encodedCert));

      String calculatedDigestValue =
          Base64.getEncoder()
              .encodeToString(MessageDigest.getInstance(getMessageDigest(digestMethod)).digest(cert.getEncoded()));

      logger.debug("calculatedDigestValue: {}", calculatedDigestValue);

      if (!digestValue.equals(calculatedDigestValue)) {
        reports.add(
            new Report(
                false,
                CONST_EL_CERT_DIGEST_VALUE,
                String.format("Value does not match, should be : %s", calculatedDigestValue)));
      }
    } catch (NoSuchAlgorithmException | CertificateException | SignatureException e) {
      reports.add(new Report(false, CONST_EL_CERT_DIGEST_VALUE, e.getMessage()));
    }
  }

  private String getMessageDigest(String digestMethod) throws SignatureException {
    switch (digestMethod) {
      case "http://www.w3.org/2000/09/xmldsig#sha1":
        return "SHA1";
      case "http://www.w3.org/2001/04/xmlenc#sha256":
        return "SHA256";
      case "http://www.w3.org/2001/04/xmlenc#sha512":
        return "SHA512";
      case "http://www.w3.org/2001/04/xmlenc#ripemd160":
        return "RIPEMD160";
      default:
        throw new SignatureException("Digest method not supported: " + digestMethod);
    }
  }

  private void validateElementAttributeValue(
      Element element,
      String elementName,
      String attributeName,
      String attributeValue,
      List<Report> reports) {

    if (!DOMUtils.checkAttributeValue(element, attributeName, attributeValue)) {
      reports.add(
          new Report(
              false, elementName, String.format("%s attribute does not match", attributeName)));
    }
  }

  private void validateElementAttributeValue(Element element, List<Report> reports) {

    if (!DOMUtils.checkAttributeValue(
        element,
        XadesValidator.CONST_ATTR_DIGEST_METHOD_ALGORITHM,
        XadesValidator.CONST_ATTR_DIGEST_METHOD_VALUES)) {
      reports.add(
          new Report(
              false,
              XadesValidator.CONST_EL_DIGEST_METHOD,
              String.format(
                  "%s attribute does not match", XadesValidator.CONST_ATTR_DIGEST_METHOD_ALGORITHM)));
    }
  }

  private void validateElementValueExist(
      String namespace, String elementName, Element parentElement, List<Report> reports) {

    Optional<Element> elementOptional =
        DOMUtils.findExactlyOneElement(namespace, elementName, parentElement);

    if (!elementOptional.isPresent()) {
      reportNotFound(reports, elementName, ELEMENT_NOT_FOUND_MESSAGE);
      return;
    }
    String content = elementOptional.get().getTextContent().trim();
    if (content.isEmpty()) {
      reports.add(new Report(false, elementName, "Element is empty"));
    }
  }

  private void validateElementAttributeExist(
      Element element, String elementName, String attributeName, List<Report> reports) {

    String attribute = element.getAttribute(attributeName);
    if (attribute.isEmpty()) {
      reportNotFound(reports, elementName, String.format("%s is empty", attributeName));
    }
  }

  private static boolean isDateInCorrectFormat(String date) {
    try {
      ZonedDateTime.parse(date, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
      return true;
    } catch (DateTimeParseException e) {
      return false;
    }
  }

  private static void reportNotFound(List<Report> reports, String elementName, String message) {
    reports.add(new Report(false, elementName, message));
  }
}
