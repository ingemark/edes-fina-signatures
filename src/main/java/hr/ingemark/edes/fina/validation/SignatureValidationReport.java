package hr.ingemark.edes.fina.validation;

import hr.ingemark.edes.fina.validation.validators.Report;
import java.util.List;
import java.util.stream.Collectors;


public class SignatureValidationReport {

    private List<Report> reports;

    private boolean valid;

    public SignatureValidationReport(List<Report> reports) {
        this.reports = reports;

        this.valid = reports
                .stream()
                .map(Report::isValid)
                .reduce(Boolean::logicalAnd)
                .orElse(true);
    }

    public boolean isValid() {
        return valid;
    }

    public String getAllErrorMessages() {
        return reports
                .stream()
                .map(Report::getErrorMessage)
                .filter(s -> s != null && !s.isEmpty())
                .collect(Collectors.joining(" | "));
    }

    public List<Report> getErrorReports() {
        return this.reports;
    }
}
