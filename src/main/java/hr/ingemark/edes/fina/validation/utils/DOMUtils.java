package hr.ingemark.edes.fina.validation.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class DOMUtils {

    private DOMUtils() {
        // util class
    }

    public static boolean checkAttributeValue(Element element, String attributeName, String attributeExpectedValue) {
        String attribute = element.getAttribute(attributeName);
        return attribute.equals(attributeExpectedValue);
    }

	public static boolean checkAttributeValue(Element element, String attributeName, List<String> attributeExpectedValues) {
		String attribute = element.getAttribute(attributeName);
		return attributeExpectedValues.contains(attribute);
	}

    public static Optional<Element> findElementByTagNameAndAttribute(
            String namespace,
            String name,
            String attributeName,
            String attributeValue,
            Document document) {
        NodeList nodes = document.getElementsByTagNameNS(namespace, name);
        return findElementByAttribute(attributeName, attributeValue, nodes);
    }

    public static Optional<Element> findExactlyOneElement(String namespace, String name, Element element) {
        NodeList nodes = element.getElementsByTagNameNS(namespace, name);
        return findExactlyOneElement(nodes);
    }


    public static Optional<Element> findExactlyOneElement(String namespace, String name, Document document) {
        NodeList nodes = document.getElementsByTagNameNS(namespace, name);
        return findExactlyOneElement(nodes);
    }

    private static Optional<Element> findExactlyOneElement(NodeList nodes) {
        if (nodes.getLength() == 1) {
            return Optional.of((Element) nodes.item(0));
        }
        return Optional.empty();
    }

    private static Optional<Element> findElementByAttribute(String attributeName, String attributeValue, NodeList nodes) {
        Objects.requireNonNull(nodes);

        for (int i = 0; i < nodes.getLength(); i++) {
            Element el = (Element) nodes.item(i);
            String fetchedAttributeValue = el.getAttribute(attributeName);
            if (fetchedAttributeValue.equals(attributeValue)) {
                return Optional.of(el);
            }
        }
        return Optional.empty();
    }
}
