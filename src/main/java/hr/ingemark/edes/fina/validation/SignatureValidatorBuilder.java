package hr.ingemark.edes.fina.validation;

import hr.ingemark.edes.fina.sig.utils.KeyStoreUtils;
import java.security.KeyStore;
import java.util.Objects;

public class SignatureValidatorBuilder {

    private KeyStore keyStore;

    private boolean acceptUnsignedDocuments = false;

    private SignatureValidatorBuilder() {
    }

    public static SignatureValidatorBuilder builder() {
        return new SignatureValidatorBuilder();
    }

    public KeystoreBuilder withKeystore() {
        return new KeystoreBuilder(this);
    }

    public SignatureValidatorBuilder acceptUnsignedDocument() {
        this.acceptUnsignedDocuments = true;
        return this;
    }

    public SignatureValidatorBuilder acceptUnsignedDocument(boolean acceptUnsignedDocuments) {
        this.acceptUnsignedDocuments = acceptUnsignedDocuments;
        return this;
    }

    public SignatureValidator build() {
        Objects.requireNonNull(keyStore);

        return new SignatureValidator(this.keyStore, this.acceptUnsignedDocuments);
    }

    public static class KeystoreBuilder {

        private String filename;

        private String password;

        private SignatureValidatorBuilder builder;

        private KeystoreBuilder(SignatureValidatorBuilder builder) {
            this.builder = builder;
        }

        public SignatureValidatorBuilder.KeystoreBuilder withFilename(String filename) {
            Objects.requireNonNull(filename);

            this.filename = filename;
            return this;
        }

        public SignatureValidatorBuilder.KeystoreBuilder withPassword(String password) {
            Objects.requireNonNull(password);

            this.password = password;
            return this;
        }

        public SignatureValidatorBuilder buildKeystore() {
            builder.keyStore = KeyStoreUtils.loadJksKeyStore(filename, password);
            return builder;
        }
    }
}
