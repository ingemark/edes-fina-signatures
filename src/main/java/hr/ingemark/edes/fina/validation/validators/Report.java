package hr.ingemark.edes.fina.validation.validators;

public class Report {

    private boolean valid;

    private String errorMessage;

    private String elementName;

    public Report(boolean valid, String elementName, String errorMessage) {
        this.valid = valid;
        this.errorMessage = errorMessage;
        this.elementName = elementName;
    }

    public boolean isValid() {
        return valid;
    }

    public String getErrorMessage() {
        return elementName + " : " + errorMessage;
    }

    public String getElementName() {
        return elementName;
    }

}
