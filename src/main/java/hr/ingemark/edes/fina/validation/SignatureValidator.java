package hr.ingemark.edes.fina.validation;

import hr.ingemark.edes.fina.sig.utils.XmlUtils;
import hr.ingemark.edes.fina.validation.ex.ValidationException;
import hr.ingemark.edes.fina.validation.validators.XadesValidator;
import java.security.KeyStore;
import java.util.Collections;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.crypto.dsig.XMLSignature;

public class SignatureValidator {

    private static final String CONST_XML_XADES_VALUE = "http://uri.etsi.org/01903/v1.3.2#";
    private static final String CONST_EL_QUALIFYING_PROPERTIES = "QualifyingProperties";
    private static final String CONST_EL_SIGNATURE = "Signature";

    private KeyStore keystore;

    private boolean acceptUnsignedDocuments;

    public SignatureValidator(KeyStore keystore, boolean acceptUnsignedDocuments) {
        this.keystore = keystore;
        this.acceptUnsignedDocuments = acceptUnsignedDocuments;
    }

    public SignatureValidationReport validate(String sourceFilename) {
        return validate(XmlUtils.loadXmlFromFile(sourceFilename));
    }

    public SignatureValidationReport validate(Document document) {
        if (isSignatureTypeXADES(document)) {
            return new XadesValidator(keystore).validate(document);
        } else {
            return handleUnknownSignature(document);
        }
    }

    private SignatureValidationReport handleUnknownSignature(Document document) {
        NodeList signatureNodeList = document.getElementsByTagNameNS(XMLSignature.XMLNS, CONST_EL_SIGNATURE);

        boolean signatureExists = signatureNodeList.getLength() > 0;

        if (signatureExists) {
            return new SignatureValidationReport(Collections.emptyList());
        } else if (acceptUnsignedDocuments) {
            return new SignatureValidationReport(Collections.emptyList());
        } else {
            throw new ValidationException("Validation FAILED. Unsigned documents are not allowed");
        }
    }

    private static boolean isSignatureTypeXADES(Document document) {
        NodeList nodes = document.getElementsByTagNameNS(CONST_XML_XADES_VALUE, CONST_EL_QUALIFYING_PROPERTIES);

        return nodes.getLength() == 1;
    }
}
