#!/bin/bash

set -e

REPO_URL=$1
REPO_USERNAME=$2
REPO_PASSWORD=$3
BUILD_NUMBER=$4

if [ -z "$REPO_URL" ]
then
  echo "Repo url not set"
  exit 1
fi

if [ -z "$REPO_USERNAME" ]
then
  echo "Repo username not set"
  exit 1
fi

if [ -z "$REPO_PASSWORD" ]
then
  echo "Repo password not set"
  exit 1
fi

if [ -z "$BUILD_NUMBER" ]
then
  echo "build number not set"
  exit 1
fi

GROUP_ID=$(mvn help:evaluate -Dexpression=project.groupId | egrep -v 'INFO|Downloading|Downloaded')
ARTIFACT_ID=$(mvn help:evaluate -Dexpression=project.artifactId | egrep -v 'INFO|Downloading|Downloaded')
VERSION="$(mvn help:evaluate -Dexpression=project.version | egrep -v 'INFO|Downloading|Downloaded').$BUILD_NUMBER"

GROUP_ID=${GROUP_ID//.//}

JAR_REPO_PATH=$REPO_URL/$GROUP_ID/$ARTIFACT_ID/$VERSION/$ARTIFACT_ID-$VERSION.jar
POM_REPO_PATH=$REPO_URL/$GROUP_ID/$ARTIFACT_ID/$VERSION/$ARTIFACT_ID-$VERSION.pom
SOURCES_REPO_PATH=$REPO_URL/$GROUP_ID/$ARTIFACT_ID/$VERSION/$ARTIFACT_ID-$VERSION-sources.jar

echo "setting version to $VERSION"
mvn versions:set -DnewVersion=$VERSION
echo "rebuilding project with version $VERSION"
mvn -Dmaven.test.skip=true clean package source:jar

curl --fail -v -u $REPO_USERNAME:$REPO_PASSWORD --upload-file pom.xml $POM_REPO_PATH
echo "uploaded jar to $JAR_REPO_PATH"

curl --fail -v -u $REPO_USERNAME:$REPO_PASSWORD --upload-file target/$ARTIFACT_ID-$VERSION.jar $JAR_REPO_PATH
echo "uploaded pom to $POM_REPO_PATH"

curl --fail -v -u $REPO_USERNAME:$REPO_PASSWORD --upload-file target/$ARTIFACT_ID-$VERSION-sources.jar $SOURCES_REPO_PATH
echo "uploaded sources to $SOURCES_REPO_PATH"
